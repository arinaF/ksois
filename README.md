# My project's README
## Sistēmas instalācija ##
### Prasības: ###
* Tomcat serveris (8 versija)
* Java Development Kit versija 8
* Maven versija 3.3.9
* MySQL DB

### Datubāze: ###
* MySQL datubāze ir nepieciešams izveidot shēmu ar nosaukumu "testdb"
* Failā "internal.properties", kas atrodas src/main/resources ir jānorāda "jdbc.url", "jdbc.username" un "jdbc.password"
* Failā "liquibase.properties", kas atrodas src/main/resources ir jānorāda "url", "username" un "password"
* Visi skripti tiks pievientoti DB ar 'liquibase' palīdzību

### Instalācija ###
* Palaist Tomcat serveri
* Atvērt Tomcat servera mājaslapu un nospiest pogu 'Manager App'
* Sadaļā 'WAR file to deploy' augšupielādēt .war failu
![deploy.png](https://bitbucket.org/repo/LoRBd6y/images/618459307-deploy.png)  
* Nospiest pogu 'Start' pie projekta 'ksois'
* Atvērt pārlūkprogrammā saiti http://localhost:8080/ksois

### Ielogošanas sistēmā ###
* Lietotājvārds: main.operator
* Parole: 12345