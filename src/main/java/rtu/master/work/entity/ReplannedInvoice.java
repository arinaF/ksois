package rtu.master.work.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "REPLANNED_INVOICES")
public class ReplannedInvoice {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(name = "INVOICE_ID", nullable = false)
    private Integer invoiceId;

    @Column(name = "COMMENT", nullable = true)
    private String comment;

    @Column(name = "REG_DATE", nullable = false)
    private Date regDate;

    @Column(name = "DELIVERY_DATE", nullable = false)
    private Date deliveryDate;

    @NotNull
    @Column(name = "OPERATOR_ID", nullable = false)
    private Integer operatorId;

    @NotNull
    @Column(name = "ROUTE_ID", nullable = false)
    private Integer routeId;

    @Column(name = "REPLANNED", nullable = false)
    private Boolean replanned;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Boolean getReplanned() {
        return replanned;
    }

    public void setReplanned(Boolean replanned) {
        this.replanned = replanned;
    }
}
