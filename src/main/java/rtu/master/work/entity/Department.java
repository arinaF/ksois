package rtu.master.work.entity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "DEPARTMENTS")
public class Department implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Column(name = "TYPE", nullable = false)
    private String type;

    @NotBlank
    @Column(name = "CODE", nullable = false)
    private String code;

    @NotBlank
    @Column(name = "REGION", nullable = false)
    private String region;

    @NotBlank
    @Column(name = "ADDRESS", nullable = false)
    private String address;

    @Column(name = "LAT", nullable = true)
    private Float lat;

    @Column(name = "LNG", nullable = true)
    private Float lng;

    @NotBlank
    @Column(name = "NAME", nullable = false)
    private String name;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Department that = (Department) o;

        if (!getId().equals(that.getId())) return false;
        if (!getType().equals(that.getType())) return false;
        if (!getCode().equals(that.getCode())) return false;
        if (!getRegion().equals(that.getRegion())) return false;
        if (!getAddress().equals(that.getAddress())) return false;
        if (getLat() != null ? !getLat().equals(that.getLat()) : that.getLat() != null) return false;
        if (getLng() != null ? !getLng().equals(that.getLng()) : that.getLng() != null) return false;
        return getName().equals(that.getName());
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getCode() != null ? getCode().hashCode() : 0);
        result = 31 * result + (getRegion() != null ? getRegion().hashCode() : 0);
        result = 31 * result + (getAddress() != null ? getAddress().hashCode() : 0);
        result = 31 * result + (getLat() != null ? getLat().hashCode() : 0);
        result = 31 * result + (getLng() != null ? getLng().hashCode() : 0);
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        return result;
    }
}
