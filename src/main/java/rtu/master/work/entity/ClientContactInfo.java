package rtu.master.work.entity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "CLIENTS_CONTACT_INFO")
public class ClientContactInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(name = "CLIENT_ID", nullable = false)
    private Integer clientId;

    @NotBlank
    @Column(name = "TYPE", nullable = false)
    private String type;

    @NotNull
    @Column(name = "CONTACT", nullable = false)
    private String contact;

    @Column(name = "CONTACT_PERSON")
    private String contactPerson;

    @Column(name = "IS_ACTIVE", nullable = false)
    private Boolean isActive;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getContactPerson() {
        return contactPerson;
    }

    public void setContactPerson(String contactPerson) {
        this.contactPerson = contactPerson;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ClientContactInfo that = (ClientContactInfo) o;

        if (getId() != null ? !getId().equals(that.getId()) : that.getId() != null) return false;
        if (getClientId() != null ? !getClientId().equals(that.getClientId()) : that.getClientId() != null)
            return false;
        if (getType() != null ? !getType().equals(that.getType()) : that.getType() != null) return false;
        if (getContact() != null ? !getContact().equals(that.getContact()) : that.getContact() != null) return false;
        if (getContactPerson() != null ? !getContactPerson().equals(that.getContactPerson()) : that.getContactPerson() != null)
            return false;
        return isActive != null ? isActive.equals(that.isActive) : that.isActive == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getClientId() != null ? getClientId().hashCode() : 0);
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        result = 31 * result + (getContact() != null ? getContact().hashCode() : 0);
        result = 31 * result + (getContactPerson() != null ? getContactPerson().hashCode() : 0);
        result = 31 * result + (isActive != null ? isActive.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "ClientContactInfo{" +
                "id=" + id +
                ", clientId=" + clientId +
                ", type='" + type + '\'' +
                ", contact='" + contact + '\'' +
                ", contactPerson='" + contactPerson + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
