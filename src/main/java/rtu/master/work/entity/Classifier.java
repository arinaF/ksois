package rtu.master.work.entity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;

@Entity
@Table(name = "CLASSIFIERS")
public class Classifier {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "PARENT_CODE", nullable = true)
    private String parentCode;

    @NotBlank
    @Column(name = "CODE", nullable = false)
    private String code;

    @NotBlank
    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "TYPE", nullable = true)
    private String type;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
        this.parentCode = parentCode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Classifier that = (Classifier) o;

        if (!getId().equals(that.getId())) return false;
        if (getParentCode() != null ? !getParentCode().equals(that.getParentCode()) : that.getParentCode() != null)
            return false;
        if (!getCode().equals(that.getCode())) return false;
        if (!getName().equals(that.getName())) return false;
        return getType() != null ? getType().equals(that.getType()) : that.getType() == null;
    }

    @Override
    public int hashCode() {
        int result = getId().hashCode();
        result = 31 * result + (getParentCode() != null ? getParentCode().hashCode() : 0);
        result = 31 * result + getCode().hashCode();
        result = 31 * result + getName().hashCode();
        result = 31 * result + (getType() != null ? getType().hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Classifier{" +
                "id=" + id +
                ", parentCode='" + parentCode + '\'' +
                ", code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
