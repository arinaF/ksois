package rtu.master.work.entity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "PROBLEMS")
public class Problem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotBlank
    @Column(name = "IDENTIFIER", nullable = false)
    private String identifier;

    @Column(name = "REG_DATE", nullable = false)
    private Date regDate;

    @NotBlank
    @Column(name = "SOURCE", nullable = false)
    private String source;

    @NotBlank
    @Column(name = "DESCRIPTION", nullable = false)
    private String description;

    @NotBlank
    @Column(name = "RESP_DEPARTMENT", nullable = false)
    private String respDepartment;

    @NotBlank
    @Column(name = "ACTION", nullable = false)
    private String action;

    @NotBlank
    @Column(name = "REASON", nullable = false)
    private String reason;

    @Column(name = "IS_INFORMED", nullable = false)
    private Boolean informed;

    @Column(name = "CLOSE_DATE", nullable = false)
    private Date closeDate;

    @Column(name = "INVOICE_ID", nullable = true)
    private Integer invoiceId;

    @Column(name = "CLIENT_ID", nullable = false)
    private Integer clientId;

    @Column(name = "PROJECT_ID", nullable = false)
    private Integer projectId;

    @Column(name = "WAREHOUSE_ID", nullable = true)
    private Integer warehouseId;

    @Column(name = "OPERATOR_ID", nullable = false)
    private Integer operatorId;

    @Column(name = "DRIVER_ID", nullable = true)
    private Integer driverId;

    @Column(name = "MANAGER_ID", nullable = true)
    private Integer managerId;

    @Column(name = "ROUTE_ID", nullable = true)
    private Integer routeID;

    @Column(name = "BACK_INV_ID", nullable = true)
    private Integer backInvId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRespDepartment() {
        return respDepartment;
    }

    public void setRespDepartment(String respDepartment) {
        this.respDepartment = respDepartment;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Boolean getInformed() {
        return informed;
    }

    public void setInformed(Boolean informed) {
        this.informed = informed;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public Integer getRouteID() {
        return routeID;
    }

    public void setRouteID(Integer routeID) {
        this.routeID = routeID;
    }

    public Integer getBackInvId() {
        return backInvId;
    }

    public void setBackInvId(Integer backInvId) {
        this.backInvId = backInvId;
    }
}