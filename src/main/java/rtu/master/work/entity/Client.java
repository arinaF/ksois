package rtu.master.work.entity;

import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Entity
@Table(name = "CLIENTS")
public class Client implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(name = "POS_NR", nullable = false)
    private Integer posNr;

    @NotBlank
    @Column(name = "REG_NR", nullable = false)
    private String regNr;

    @NotBlank
    @Column(name = "NAME", nullable = false)
    private String name;

    @NotBlank
    @Column(name = "ADDRESS", nullable = false)
    private String address;

    @Column(name = "LAT")
    private Float lat;

    @Column(name = "LNG")
    private Float lng;

    @Column(name = "REGION")
    private String region;

    @Column(name = "IS_ACTIVE", nullable = false)
    private Boolean isActive;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPosNr() {
        return posNr;
    }

    public void setPosNr(Integer posNr) {
        this.posNr = posNr;
    }

    public String getRegNr() {
        return regNr;
    }

    public void setRegNr(String regNr) {
        this.regNr = regNr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        if (getId() != null ? !getId().equals(client.getId()) : client.getId() != null) return false;
        if (getPosNr() != null ? !getPosNr().equals(client.getPosNr()) : client.getPosNr() != null) return false;
        if (getRegNr() != null ? !getRegNr().equals(client.getRegNr()) : client.getRegNr() != null) return false;
        if (getName() != null ? !getName().equals(client.getName()) : client.getName() != null) return false;
        if (getAddress() != null ? !getAddress().equals(client.getAddress()) : client.getAddress() != null)
            return false;
        if (getLat() != null ? !getLat().equals(client.getLat()) : client.getLat() != null) return false;
        if (getLng() != null ? !getLng().equals(client.getLng()) : client.getLng() != null) return false;
        if (getRegion() != null ? !getRegion().equals(client.getRegion()) : client.getRegion() != null) return false;
        return isActive != null ? isActive.equals(client.isActive) : client.isActive == null;
    }

    @Override
    public int hashCode() {
        int result = getId() != null ? getId().hashCode() : 0;
        result = 31 * result + (getPosNr() != null ? getPosNr().hashCode() : 0);
        result = 31 * result + (getRegNr() != null ? getRegNr().hashCode() : 0);
        result = 31 * result + (getName() != null ? getName().hashCode() : 0);
        result = 31 * result + (getAddress() != null ? getAddress().hashCode() : 0);
        result = 31 * result + (getLat() != null ? getLat().hashCode() : 0);
        result = 31 * result + (getLng() != null ? getLng().hashCode() : 0);
        result = 31 * result + (getRegion() != null ? getRegion().hashCode() : 0);
        result = 31 * result + (isActive != null ? isActive.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", posNr=" + posNr +
                ", regNr='" + regNr + '\'' +
                ", name='" + name + '\'' +
                ", address='" + address + '\'' +
                ", lat=" + lat +
                ", lng=" + lng +
                ", region='" + region + '\'' +
                ", isActive=" + isActive +
                '}';
    }
}
