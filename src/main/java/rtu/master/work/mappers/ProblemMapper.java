package rtu.master.work.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import rtu.master.work.dto.ProblemDto;
import rtu.master.work.dto.ProblemShortDto;
import rtu.master.work.entity.Problem;

@Mapper(componentModel = "spring")
public interface ProblemMapper {

    ProblemMapper INSTANCE = Mappers.getMapper(ProblemMapper.class);

    ProblemDto problemToProblemDto(Problem problem);

    Problem problemDtoToProblem(ProblemDto problemDto);

    ProblemShortDto problemToProblemShortDto(Problem problem);

}
