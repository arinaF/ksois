package rtu.master.work.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import rtu.master.work.dto.ClientDto;
import rtu.master.work.entity.Client;

@Mapper(componentModel = "spring")
public interface ClientMapper {

    ClientMapper INSTANCE = Mappers.getMapper(ClientMapper.class);

    @Mappings({
            @Mapping(target = "region.code", source = "region")
    })
    ClientDto clientToClientDto(Client client);

    @Mappings({
            @Mapping(target = "region", source = "region.code")
    })
    Client clientDtoToClient(ClientDto clientDto);
}
