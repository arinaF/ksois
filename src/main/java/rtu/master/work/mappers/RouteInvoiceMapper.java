package rtu.master.work.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import rtu.master.work.dto.RouteInvoiceDto;
import rtu.master.work.entity.RouteInvoice;

import java.util.List;

@Mapper(componentModel = "spring")
public interface RouteInvoiceMapper {

    RouteInvoiceMapper INSTANCE = Mappers.getMapper(RouteInvoiceMapper.class);

    RouteInvoiceDto routeInvoiceToRouteInvoiceDto(RouteInvoice routeInvoice);

    RouteInvoice routeInvoiceDtoToLinkRouteInvoice(RouteInvoiceDto routeInvoiceDto);

    List<RouteInvoiceDto> routeInvoiceListToRouteInvoiceDtoList(List<RouteInvoice> routeInvoice);

    List<RouteInvoice> routeInvoiceDtoListToLinkRouteInvoiceList(List<RouteInvoiceDto> routeInvoiceDto);

}
