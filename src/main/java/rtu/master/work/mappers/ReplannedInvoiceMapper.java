package rtu.master.work.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import rtu.master.work.dto.ReplannedInvoiceDto;
import rtu.master.work.entity.ReplannedInvoice;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ReplannedInvoiceMapper {

    ReplannedInvoiceMapper INSTANCE = Mappers.getMapper(ReplannedInvoiceMapper.class);

    ReplannedInvoiceDto replannedInvoiceToReplannedInvoiceDto(ReplannedInvoice replannedInvoice);

    ReplannedInvoice replannedInvoiceDtoToReplannedInvoice(ReplannedInvoiceDto replannedInvoiceDto);

    List<ReplannedInvoiceDto> replannedInvoiceListToReplannedInvoiceDtoList(List<ReplannedInvoice> replannedInvoiceList);

    List<ReplannedInvoice> replannedInvoiceDtoListToReplannedInvoiceList(List<ReplannedInvoiceDto> replannedInvoiceDtoList);

}
