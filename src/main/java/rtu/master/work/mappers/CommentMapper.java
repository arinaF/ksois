package rtu.master.work.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import rtu.master.work.dto.CommentDto;
import rtu.master.work.entity.Comment;

@Mapper(componentModel = "spring")
public interface CommentMapper {

    CommentMapper INSTANCE = Mappers.getMapper(CommentMapper.class);

    CommentDto commentToCommentDto(Comment comment);

    Comment commentDtoToComment(CommentDto commentDto);
}
