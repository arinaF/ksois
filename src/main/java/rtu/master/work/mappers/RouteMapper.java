package rtu.master.work.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;
import rtu.master.work.dto.RouteDto;
import rtu.master.work.entity.Route;

@Mapper(componentModel = "spring")
public interface RouteMapper {

    RouteMapper INSTANCE = Mappers.getMapper(RouteMapper.class);

    Route routeDtoToRoute(RouteDto routeDto);

    RouteDto routeToRouteDto(Route route);
}
