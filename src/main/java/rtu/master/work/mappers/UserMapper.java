package rtu.master.work.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import rtu.master.work.dto.UserDto;
import rtu.master.work.entity.UserInfo;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mappings({
            @Mapping(target = "userId", source = "id")
    })
    UserDto userInfoToUserDto(UserInfo userInfo);

    @Mappings({
            @Mapping(target = "id", source = "userId")
    })
    UserInfo userDtoToUserInfo(UserDto userDto);
}
