package rtu.master.work.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.factory.Mappers;
import rtu.master.work.dto.EmployeeDto;
import rtu.master.work.entity.Employee;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {

    EmployeeMapper INSTANCE = Mappers.getMapper(EmployeeMapper.class);

    @Mappings({
            @Mapping(target = "position.code", source = "position"),
            @Mapping(target = "region.code", source = "region")
    })
    EmployeeDto employeeToEmployeeDto(Employee employee);

    @Mappings({
            @Mapping(target = "position", source = "position.code"),
            @Mapping(target = "departId", source = "department.id"),
            @Mapping(target = "region", source = "region.code")
    })
    Employee employeeDtoToEmployee(EmployeeDto employeeDto);
}
