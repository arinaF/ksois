package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.dao.ProblemDao;
import rtu.master.work.dto.ProblemDto;
import rtu.master.work.dto.ProblemShortDto;
import rtu.master.work.entity.*;
import rtu.master.work.mappers.ProblemMapper;

import java.text.SimpleDateFormat;
import java.util.*;

@Service("problemService")
@Transactional
public class ProblemServiceImpl implements ProblemService {

    @Autowired
    private ProblemDao dao;

    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private ClientService clientService;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private InvoiceService invoiceService;

    @Autowired
    private ClassifierService classifierService;

    @Autowired
    private ContactInfoService contactInfoService;

    @Autowired
    private EmailService emailService;

    @Autowired
    private UserService userService;

    @Override
    public List<Problem> findAllProblems() {
        return dao.findAllProblems();
    }

    @Override
    public List<Problem> findAllOperatorProblems(Integer operatorId) {
        return dao.findAllOperatorProblems(operatorId);
    }

    @Override
    public List<ProblemShortDto> findAllDriverProblems(Integer driverId) {
        List<ProblemShortDto> problems = new ArrayList<>();
        dao.findAllDriverProblems(driverId).forEach(p -> {
            problems.add(findFullProblemShort(p.getId()));
        });
        return problems;
    }

    @Override
    public List<ProblemShortDto> findAllClientProblems(Integer clientId) {
        List<ProblemShortDto> problems = new ArrayList<>();
        dao.findAllClientProblems(clientId).forEach(p -> {
            problems.add(findFullProblemShort(p.getId()));
        });
        return problems;
    }

    @Override
    public Problem findProblem(Integer id) {
        return dao.findProblem(id);
    }

    @Override
    public ProblemDto findFullProblem(Integer id) {
        Problem problem = findProblem(id);
        if (problem != null) {
            ProblemDto problemDto = ProblemMapper.INSTANCE.problemToProblemDto(problem);
            Client client = clientService.findClient(problem.getClientId());
            Project project = projectService.findProject(problem.getProjectId());
            Department warehouse = departmentService.findDepartment(problem.getWarehouseId());
            Employee operator = employeeService.findEmployee(problem.getOperatorId());
            Employee driver = employeeService.findEmployee(problem.getDriverId());
            Employee manager = employeeService.findEmployee(problem.getManagerId());
            Invoice invoice = invoiceService.findInvoice(problem.getInvoiceId());
            Classifier source = classifierService.findClassifier(problem.getSource());
            Classifier action = classifierService.findClassifier(problem.getAction());
            Classifier reason = classifierService.findClassifier(problem.getReason());
            //ToDo get info about Route, BackInvoice, Comments

            if (client != null) {
                problemDto.setClientAddress(client.getAddress());
                problemDto.setClientName(client.getName());
                problemDto.setClientPosNr(client.getPosNr());
            }

            if (project != null) {
                problemDto.setProjectCode(project.getCode());
            }

            if (warehouse != null) {
                problemDto.setWarehouseName(warehouse.getName());
            }

            if (operator != null) {
                problemDto.setOperatorName(operator.getName());
                problemDto.setOperatorSurname(operator.getSurname());
            }

            if (driver != null) {
                problemDto.setDriverName(driver.getName());
                problemDto.setDriverSurname(driver.getSurname());
            }

            if (manager != null) {
                problemDto.setManagerName(manager.getName());
                problemDto.setManagerSurname(manager.getSurname());
            }

            if (invoice != null) {
                problemDto.setInvoiceDate(invoice.getDate());
                problemDto.setInvoiceIdentifier(invoice.getIdentifier());
            }

            if (action != null) {
                problemDto.setActionName(action.getName());
            }

            if (reason != null) {
                problemDto.setReasonName(reason.getName());
            }

            if (source != null) {
                problemDto.setSourceName(source.getName());
            }
            return problemDto;
        }
        return null;
    }

    @Override
    public ProblemShortDto findFullProblemShort(Integer id) {
        Problem problem = findProblem(id);
        if (problem != null) {
            ProblemShortDto problemShortDto = ProblemMapper.INSTANCE.problemToProblemShortDto(problem);
            Client client = clientService.findClient(problem.getClientId());
            Invoice invoice = invoiceService.findInvoice(problem.getInvoiceId());
            Classifier source = classifierService.findClassifier(problem.getSource());
            Classifier action = classifierService.findClassifier(problem.getAction());
            Classifier reason = classifierService.findClassifier(problem.getReason());
            if (client != null) {
                problemShortDto.setClientAddress(client.getAddress());
                problemShortDto.setClientName(client.getName());
                problemShortDto.setClientPosNr(client.getPosNr());
            }
            if (invoice != null) {
                problemShortDto.setInvoiceIdentifier(invoice.getIdentifier());
            }

            if (source != null) {
                problemShortDto.setSourceName(source.getName());
            }

            if (action != null) {
                problemShortDto.setActionName(action.getName());
            }

            if (reason != null) {
                problemShortDto.setReasonName(reason.getName());
            }
            return problemShortDto;
        }
        return null;
    }

    @Override
    public List<ProblemShortDto> findAllFullProblems() {
        List<Problem> problems = findAllProblems();
        List<ProblemShortDto> fullProblems = new ArrayList<>();
        problems.forEach(p -> {
            fullProblems.add(findFullProblemShort(p.getId()));
        });
        return fullProblems;
    }

    @Override
    public List<ProblemShortDto> findAllOperatorFullProblems(Integer operatorId) {
        List<Problem> operatorProblems = findAllOperatorProblems(operatorId);
        List<ProblemShortDto> operatorFullProblems = new ArrayList<>();
        operatorProblems.forEach(p -> {
            operatorFullProblems.add(findFullProblemShort(p.getId()));
        });
        return operatorFullProblems;
    }

    @Override
    public Problem saveProblem(ProblemDto problemDto) {
        Problem problem = ProblemMapper.INSTANCE.problemDtoToProblem(problemDto);
        setOperatorAndIdentifier(problem);
        dao.saveProblem(problem);
        if (problemDto.getInformed()) {
            emailService.sendEmail(problemDto);
        }
        return problem;
    }

    @Override
    public void updateProblem(Problem problem) {
        Problem entity = findProblem(problem.getId());
        if (entity != null) {
            entity.setIdentifier(problem.getIdentifier());
            entity.setRegDate(problem.getRegDate());
            entity.setSource(problem.getSource());
            entity.setDescription(problem.getDescription());
            entity.setRespDepartment(problem.getRespDepartment());
            entity.setAction(problem.getAction());
            entity.setReason(problem.getReason());
            entity.setInformed(problem.getInformed());
            entity.setCloseDate(problem.getCloseDate());
            entity.setInvoiceId(problem.getInvoiceId());
            entity.setClientId(problem.getClientId());
            entity.setProjectId(problem.getProjectId());
            entity.setWarehouseId(problem.getWarehouseId());
            entity.setOperatorId(problem.getOperatorId());
            entity.setDriverId(problem.getDriverId());
            entity.setManagerId(problem.getManagerId());
            entity.setRouteID(problem.getRouteID());
            entity.setBackInvId(problem.getBackInvId());
        }
    }

    @Override
    public void updateProblem(ProblemDto problemDto) {
        Problem problem = ProblemMapper.INSTANCE.problemDtoToProblem(problemDto);
        updateProblem(problem);
    }

    @Override
    public void deleteProblem(Integer id) {
        dao.deleteProblem(id);
    }

    @Override
    public Map<String, Object> getProblemDataByInvoice(String invoiceIdentifier) {
        Map<String, Object> map = new HashMap<>();
        Invoice invoice = invoiceService.findInvoice(invoiceIdentifier);
        if (invoice != null) {
            Client client = clientService.findClient(invoice.getClientId());
            Project project = projectService.findProject(invoice.getProjectId());
            Department warehouse = departmentService.findDepartment(invoice.getWarehouseId());
            //ToDo add info about driver and manager

            map.put("invoice", invoice);
            map.put("client", client);
            map.put("project", project);
            map.put("warehouse", warehouse);
        }
        return map;
    }

    @Override
    public Map<String, Object> getProblemDataByPosNr(String posNr) {
        Map<String, Object> map = new HashMap<>();
        Client client = clientService.findByPosNr(Integer.valueOf(posNr));
        map.put("client", client);
        return map;
    }

    private void setOperatorAndIdentifier(Problem problem) {
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserInfo user = userService.findUserByUserName(principal.getUsername());

        Integer operatorId = employeeService.findByUserId(user.getId()).getId();

        String username = user.getUsername();
        int idx = username.lastIndexOf(".");
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Integer count = dao.currentDaysProblemCount(operatorId);

        StringBuilder identifier = new StringBuilder();
        identifier.append(username.substring(0, idx).substring(0, 1).toUpperCase());
        identifier.append(username.substring(idx).substring(1, 2).toUpperCase());
        identifier.append("-");
        identifier.append(dateFormat.format(new Date()));
        identifier.append("-");
        identifier.append(count + 1);

        problem.setOperatorId(operatorId);
        problem.setIdentifier(identifier.toString());
    }

}