package rtu.master.work.service;

import rtu.master.work.dto.RouteDto;
import rtu.master.work.entity.Route;

import java.util.List;

public interface RouteService {

    Route findRoute(Integer id);

    RouteDto findFullRoute(Integer id);

    List<RouteDto> findAllRoutes();

    List<RouteDto> findAllCurrentDayRoutes();

    List<RouteDto> findDriverRoutes(Integer driverId);

    void updateRoute(RouteDto routeDto);
}
