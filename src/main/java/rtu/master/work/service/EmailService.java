package rtu.master.work.service;

import rtu.master.work.dto.ProblemDto;

public interface EmailService {

    void sendEmail(ProblemDto problem);
}
