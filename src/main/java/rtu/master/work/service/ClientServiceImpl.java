package rtu.master.work.service;

import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.errors.ApiException;
import com.google.maps.model.GeocodingResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import rtu.master.work.dao.ClientDao;
import rtu.master.work.dto.ClientDto;
import rtu.master.work.entity.Classifier;
import rtu.master.work.entity.Client;
import rtu.master.work.entity.ClientContactInfo;
import rtu.master.work.mappers.ClientMapper;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service("clientService")
@Transactional
public class ClientServiceImpl implements ClientService {

    @Autowired
    private ClientDao dao;

    @Autowired
    private ClientContactInfoService clientContactInfoService;

    @Autowired
    private ClassifierService classifierService;

    private static final GeoApiContext CONTEXT = new GeoApiContext().setApiKey("AIzaSyDUd1jcxLQgBhlzkNJt6m8InAkfiYCkcUg");

    @Override
    public List<Client> findAllClients() {
        return dao.findAllClients();
    }

    @Override
    public Client findClient(Integer id) {
        return dao.findClient(id);
    }

    @Override
    public Client findByPosNr(Integer posNr) {
        return dao.findByPosNr(posNr);
    }

    @Override
    public ClientDto findFullClient(Integer id) {
        Client client = findClient(id);
        return findFullClient(client);
    }

    @Override
    public ClientDto findFullClient(Client client) {
        if (client != null) {
            ClientDto clientDto = ClientMapper.INSTANCE.clientToClientDto(client);
            Classifier region = classifierService.findClassifier(client.getRegion());
            List<ClientContactInfo> clientContactInfoList = clientContactInfoService.findClientContactInfo(client.getId());

            clientDto.setRegion(region);
            clientDto.setClientContactInfoList(clientContactInfoList);
            return clientDto;
        }
        return new ClientDto();
    }

    @Override
    public List<ClientDto> findFullClients() {
        List<Client> clients = findAllClients();
        List<ClientDto> fullClients = new ArrayList<>();
        clients.forEach(c -> {
            fullClients.add(findFullClient(c));
        });
        return fullClients;
    }

    @Override
    public Client saveClient(ClientDto clientDto) {
        Client client = ClientMapper.INSTANCE.clientDtoToClient(clientDto);
        setGeoData(client);
        Client savedClient = dao.saveClient(client);
        if (savedClient != null && savedClient.getId() != null) {
            clientDto.getClientContactInfoList().forEach(c -> c.setClientId(savedClient.getId()));
            clientContactInfoService.saveOrUpdateClientContactInfo(clientDto.getClientContactInfoList());
        }
        return savedClient;
    }

    @Override
    public void updateClient(Client client) {
        Client entity = findClient(client.getId());
        setGeoData(client);
        if (entity != null) {
            entity.setPosNr(client.getPosNr());
            entity.setRegNr(client.getRegNr());
            entity.setName(client.getName());
            entity.setAddress(client.getAddress());
            entity.setLat(client.getLat());
            entity.setLng(client.getLng());
            entity.setActive(client.getActive());
        }
    }

    @Override
    public void updateClient(ClientDto clientDto) {
        Client client = ClientMapper.INSTANCE.clientDtoToClient(clientDto);
        updateClient(client);
        clientContactInfoService.saveOrUpdateClientContactInfo(clientDto.getClientContactInfoList());
    }

    @Override
    public void deleteClient(Integer clientId) {
        clientContactInfoService.deleteClientContactInfo(clientId);
        dao.deleteClient(clientId);
    }

    private void setGeoData(Client client) {
        GeocodingResult[] results = new GeocodingResult[0];
        try {
            results = GeocodingApi.geocode(CONTEXT, client.getAddress()).await();
        } catch (ApiException | InterruptedException | IOException e) {
            e.printStackTrace();
        }
        if (results.length > 0 && results[0] != null && results[0].geometry != null && results[0].geometry.location != null) {
            client.setLat((float) results[0].geometry.location.lat);
            client.setLng((float) results[0].geometry.location.lng);
        }
    }
}
