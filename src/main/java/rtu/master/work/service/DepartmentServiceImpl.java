package rtu.master.work.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.dao.DepartmentDao;
import rtu.master.work.entity.Department;

import java.util.List;
import java.util.stream.Collectors;

@Service("departmentService")
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    DepartmentDao dao;

    @Override
    public Department findDepartment(Integer id) {
        return dao.findDepartment(id);
    }

    @Override
    public List<Department> findAllDepartments() {
        return dao.findDepartments();
    }

    @Override
    public List<Department> findAllDepartments(String type, String code) {
        if (StringUtils.isNotBlank(code)) {
            return dao.findDepartments()
                    .stream()
                    .filter(d -> (type.equals(d.getType()) && code.equals(d.getCode())))
                    .collect(Collectors.toList());
        }
        return dao.findDepartments()
                .stream()
                .filter(d -> type.equals(d.getType()))
                .collect(Collectors.toList());
    }
}
