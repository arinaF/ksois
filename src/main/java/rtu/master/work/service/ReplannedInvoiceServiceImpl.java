package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import rtu.master.work.dao.ReplannedInvoiceDao;
import rtu.master.work.dto.ReplannedInvoiceDto;
import rtu.master.work.dto.RouteInvoiceDto;
import rtu.master.work.entity.*;
import rtu.master.work.mappers.ReplannedInvoiceMapper;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Service("replannedInvoiceService")
@Transactional
public class ReplannedInvoiceServiceImpl implements ReplannedInvoiceService {

    @Autowired
    ReplannedInvoiceDao replannedInvoiceDao;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    RouteService routeService;

    @Autowired
    ClientService clientService;

    @Autowired
    UserService userService;

    @Override
    public List<ReplannedInvoiceDto> findAllReplannedInvoices() {
        List<ReplannedInvoice> invoices = replannedInvoiceDao.findAllReplannedInvoices();
        List<ReplannedInvoiceDto> invoiceDtoList =
                ReplannedInvoiceMapper.INSTANCE
                        .replannedInvoiceListToReplannedInvoiceDtoList(invoices);
        invoiceDtoList.forEach(inv -> {
            Invoice invoice = invoiceService.findInvoice(inv.getInvoiceId());
            Employee operator = employeeService.findEmployee(inv.getOperatorId());
            String routeCode = routeService.findRoute(inv.getRouteId()).getCode();
            Client client = clientService.findClient(invoice.getClientId());

            inv.setInvoiceIdentifier(invoice.getIdentifier());
            inv.setOperatorName(operator.getName());
            inv.setOperatorSurname(operator.getSurname());
            inv.setRouteCode(routeCode);
            inv.setClientId(client.getId());
            inv.setClientName(client.getName());
            inv.setClientAddress(client.getAddress());
            inv.setClientPosNr(client.getPosNr());
        });


        return invoiceDtoList;
    }

    @Override
    public void updateReplannedInvoice(ReplannedInvoiceDto replannedInvoiceDto) {
        ReplannedInvoice entity = replannedInvoiceDao.findReplannedInvoice(replannedInvoiceDto.getId());
        if (entity != null) {
            entity.setComment(replannedInvoiceDto.getComment());
            entity.setDeliveryDate(replannedInvoiceDto.getDeliveryDate());
        }
    }

    @Override
    public void saveReplannedInvoices(List<RouteInvoiceDto> routeInvoices, String username) {
        if (!CollectionUtils.isEmpty(routeInvoices)) {
            routeInvoices.stream().filter(i -> i.getSelected() != null && i.getSelected()).forEach(inv -> {

                Integer userId = userService.findUserByUserName(username).getId();
                Integer operatorId = employeeService.findByUserId(userId).getId();
                Date date = new Date();
                Calendar c = Calendar.getInstance();
                c.setTime(date);
                c.add(Calendar.DATE, 1);
                date = c.getTime();
                if (replannedInvoiceDao.countReplannedInvoices(inv.getInvoiceId(), date, inv.getRouteId()) < 1) {
                    ReplannedInvoice invoice = new ReplannedInvoice();
                    invoice.setInvoiceId(inv.getInvoiceId());
                    invoice.setComment(inv.getComment());
                    invoice.setRegDate(new Date());
                    invoice.setDeliveryDate(date);
                    invoice.setOperatorId(operatorId);
                    invoice.setRouteId(inv.getRouteId());
                    replannedInvoiceDao.saveReplannedInvoice(invoice);
                }

            });
        }
    }

    @Override
    public void deleteReplannedInvoice(Integer id) {
        replannedInvoiceDao.deleteReplannedInvoice(id);
    }
}
