package rtu.master.work.service;

import rtu.master.work.entity.Project;

import java.util.List;

public interface ProjectService {

    Project findProject(Integer id);

    List<Project> findAllProjects();

    void updateOrSaveProject(Project project);

    void deleteProject(Integer id);
}
