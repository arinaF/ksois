package rtu.master.work.service;

import rtu.master.work.dto.EmployeeDto;
import rtu.master.work.entity.Employee;

import java.util.List;

public interface EmployeeService {

    List<Employee> findAllEmployees(Integer departId);

    Employee findEmployee(Integer id);

    EmployeeDto findByUserId(Integer userId);

    EmployeeDto findFullEmployee(Integer id);

    EmployeeDto findFullEmployee(Employee employee);

    List<EmployeeDto> findFullEmployees(String departmentType, String departmentCode, Integer departmentId);

    Employee saveEmployee(EmployeeDto employeeDto);

    void updateEmployee(Employee employee);

    void updateEmployee(EmployeeDto employeeDto);

    void deleteEmployee(Integer id);
}

