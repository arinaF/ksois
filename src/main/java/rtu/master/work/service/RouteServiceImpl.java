package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.dao.RouteDao;
import rtu.master.work.dto.RouteDto;
import rtu.master.work.entity.Classifier;
import rtu.master.work.entity.Employee;
import rtu.master.work.entity.Route;
import rtu.master.work.mappers.RouteMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("routeService")
@Transactional
public class RouteServiceImpl implements RouteService {

    @Autowired
    RouteDao routeDao;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    ClassifierService classifierService;

    @Override
    public Route findRoute(Integer id) {
        return routeDao.findRoute(id);
    }

    @Override
    public RouteDto findFullRoute(Integer id) {
        RouteDto routeDto = RouteMapper.INSTANCE.routeToRouteDto(findRoute(id));
        Employee driver = employeeService.findEmployee(routeDto.getDriverId());
        Classifier region = classifierService.findClassifier(routeDto.getRegion());

        routeDto.setDriverName(driver.getName());
        routeDto.setDriverSurname(driver.getSurname());
        routeDto.setRegionName(region.getName());
        return routeDto;
    }

    @Override
    public List<RouteDto> findAllRoutes() {
        List<RouteDto> routes = new ArrayList<>();
        routeDao.findAllRoutes()
                .stream()
                .map(Route::getId)
                .collect(Collectors.toList())
                .forEach(id -> routes.add(findFullRoute(id)));
        return routes;
    }

    @Override
    public List<RouteDto> findAllCurrentDayRoutes() {
        List<RouteDto> routes = new ArrayList<>();
        routeDao.findAllCurrentDayRoutes()
                .stream()
                .map(Route::getId)
                .collect(Collectors.toList())
                .forEach(id -> routes.add(findFullRoute(id)));
        return routes;
    }

    @Override
    public List<RouteDto> findDriverRoutes(Integer driverId) {
        List<RouteDto> routes = new ArrayList<>();
        routeDao.findDriverRoutes(driverId)
                .stream()
                .map(Route::getId)
                .collect(Collectors.toList())
                .forEach(id -> routes.add(findFullRoute(id)));
        return routes;
    }

    @Override
    public void updateRoute(RouteDto routeDto) {
        Route entity = findRoute(routeDto.getId());
        if (entity != null) {
            entity.setDriverId(routeDto.getDriverId());
        }
    }
}
