package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.dao.RouteInvoiceDao;
import rtu.master.work.dto.RouteInvoiceDto;
import rtu.master.work.entity.Client;
import rtu.master.work.entity.Invoice;
import rtu.master.work.entity.Project;
import rtu.master.work.entity.RouteInvoice;
import rtu.master.work.mappers.RouteInvoiceMapper;
import rtu.master.work.mappers.RouteMapper;

import java.util.List;

@Service("routeInvoiceService")
@Transactional
public class RouteInvoiceServiceImpl implements RouteInvoiceService{

    @Autowired
    RouteInvoiceDao routeInvoiceDao;

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    ClientService clientService;

    @Autowired
    ProjectService projectService;


    @Override
    public List<RouteInvoiceDto> findRouteInvoices(Integer routeId) {
        List<RouteInvoiceDto> routeInvoices = RouteInvoiceMapper.INSTANCE
                .routeInvoiceListToRouteInvoiceDtoList(routeInvoiceDao.findRouteInvoices(routeId));
        routeInvoices.forEach(ri -> {
            Invoice invoice = invoiceService.findInvoice(ri.getInvoiceId());
            Client client = clientService.findClient(invoice.getClientId());
            Project project = projectService.findProject(invoice.getProjectId());

            ri.setInvoiceIdentifier(invoice.getIdentifier());
            ri.setClientId(client.getId());
            ri.setClientPosNr(client.getPosNr());
            ri.setClientName(client.getName());
            ri.setClientAddress(client.getAddress());
            ri.setLat(client.getLat());
            ri.setLng(client.getLng());
            ri.setProject(project.getCode());
        });
        return routeInvoices;
    }
}
