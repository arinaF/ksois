package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.dao.ProjectDao;
import rtu.master.work.entity.Project;

import java.util.List;

@Service("projectService")
@Transactional
public class ProjectServiceImpl implements ProjectService {

    @Autowired
    ProjectDao dao;

    @Override
    public Project findProject(Integer id) {
        return dao.findProject(id);
    }

    @Override
    public List<Project> findAllProjects() {
        return dao.findAllProjects();
    }

    @Override
    public void updateOrSaveProject(Project project) {
        if (project != null) {
            if (project.getId() == null) {
                saveProject(project);
            } else {
                updateProject(project);
            }
        }
    }

    @Override
    public void deleteProject(Integer id) {
        dao.deleteProject(id);
    }

    private void saveProject(Project project) {
        dao.saveProject(project);
    }

    private void updateProject(Project project) {
        Project entity = findProject(project.getId());
        if (entity != null) {
            entity.setCode(project.getCode());
            entity.setDescription(project.getDescription());
        }
    }
}
