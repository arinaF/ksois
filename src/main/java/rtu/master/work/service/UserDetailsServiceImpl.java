package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import rtu.master.work.entity.UserInfo;

import java.util.HashSet;
import java.util.Set;


@Component
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        UserInfo user = userService.findUserByUserName(username);

        if (user == null) {
            throw new UsernameNotFoundException("No user found with name " + username);
        }

        Set<GrantedAuthority> authorityList = new HashSet<GrantedAuthority>();
        authorityList.add(new SimpleGrantedAuthority(user.getRole()));

        return new User(user.getUsername(), user.getPassword(), authorityList);
    }
}
