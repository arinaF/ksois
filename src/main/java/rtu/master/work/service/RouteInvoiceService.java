package rtu.master.work.service;

import rtu.master.work.dto.RouteInvoiceDto;

import java.util.List;

public interface RouteInvoiceService {

    List<RouteInvoiceDto> findRouteInvoices(Integer routeId);
}
