package rtu.master.work.service;

import rtu.master.work.dto.CommentDto;
import rtu.master.work.entity.Comment;

import java.util.List;

public interface CommentService {

    List<CommentDto> findAllComments(Integer problemId);

    Comment findComment(Integer id);

    void saveOrUpdateComment(CommentDto commentDto, String username);

    void deleteComment(Integer id);
}
