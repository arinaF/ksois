package rtu.master.work.service;

import rtu.master.work.dto.UserDto;
import rtu.master.work.entity.UserInfo;

import java.util.List;

public interface UserService {

    UserInfo findUserByUserName(String username);

    List<UserDto> findAllUsers();

    UserInfo findUser(Integer id);

    UserInfo saveUser(UserDto userDto);

    void updateUserInfo(UserDto userDto);

    void deleteUserAndUpdateEmployee(Integer userId);

    void deleteUser(Integer userId);
}
