package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.dao.ClassifierDao;
import rtu.master.work.entity.Classifier;

import java.util.List;

@Service("classifierService")
@Transactional
public class ClassifierServiceImpl implements ClassifierService {

    @Autowired
    private ClassifierDao dao;

    @Override
    public List<Classifier> findAllClassifiers(String parentCode) {
        return dao.findAllClassifiers(parentCode);
    }

    @Override
    public List<Classifier> findAllClassifiers(String parentCode, String type) {
        return dao.findAllClassifiers(parentCode, type);
    }

    @Override
    public Classifier findClassifier(String code) {
        return dao.findClassifier(code);
    }
}
