package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.dao.EmployeeDao;
import rtu.master.work.dto.EmployeeDto;
import rtu.master.work.entity.Classifier;
import rtu.master.work.entity.Department;
import rtu.master.work.entity.Employee;
import rtu.master.work.entity.EmployeeContactInfo;
import rtu.master.work.mappers.EmployeeMapper;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("employeeService")
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeDao dao;

    @Autowired
    private ClassifierService classifierService;

    @Autowired
    private ContactInfoService contactInfoService;

    @Autowired
    private DepartmentService departmentService;

    @Autowired
    private UserService userService;

    public List<Employee> findAllEmployees(Integer departId) {
        return dao.findAllEmployees(departId);
    }

    public Employee findEmployee(Integer id) {
        if(id != null){
            return dao.findEmployee(id);
        }
        return null;
    }

    @Override
    public EmployeeDto findByUserId(Integer userId) {
        Employee employee = dao.findByUserId(userId);
        return findFullEmployee(employee);
    }

    public EmployeeDto findFullEmployee(Integer id) {
        Employee employee = findEmployee(id);
        return findFullEmployee(employee);
    }

    public EmployeeDto findFullEmployee(Employee employee) {
        if (employee != null) {
            EmployeeDto fullEmployee = EmployeeMapper.INSTANCE.employeeToEmployeeDto(employee);

            Classifier region = classifierService.findClassifier(employee.getRegion());
            Classifier position = classifierService.findClassifier(employee.getPosition());
            Department department = departmentService.findDepartment(employee.getDepartId());
            List<EmployeeContactInfo> employeeContactInfoList = contactInfoService.findEmployeeContactInfo(employee.getId());

            fullEmployee.setRegion(region);
            fullEmployee.setPosition(position);
            fullEmployee.setDepartment(department);
            fullEmployee.setEmployeeContactInfoList(employeeContactInfoList);
            return fullEmployee;
        }
        return new EmployeeDto();
    }

    public List<EmployeeDto> findFullEmployees(String departmentType, String departmentCode, Integer departmentId) {
        List<Employee> employees = new ArrayList<>();
        List<EmployeeDto> employeeDtoList = new ArrayList<>();
        List<Integer> departmentIds = new ArrayList<>();
        if (departmentId != null) {
            departmentIds.add(departmentId);
        } else {
            departmentIds = departmentService.findAllDepartments(departmentType, departmentCode)
                    .stream()
                    .map(Department::getId)
                    .collect(Collectors.toList());
        }
        departmentIds.forEach(id -> employees.addAll(findAllEmployees(id)));

        employees.forEach(empl -> employeeDtoList.add(findFullEmployee(empl)));
        return employeeDtoList;
    }

    @Override
    public Employee saveEmployee(EmployeeDto employeeDto) {
        Employee employee = EmployeeMapper.INSTANCE.employeeDtoToEmployee(employeeDto);
        Employee savedEmployee = dao.saveEmployee(employee);
        if (savedEmployee != null && savedEmployee.getId() != null) {
            employeeDto.getEmployeeContactInfoList().forEach(c -> c.setEmployeeId(savedEmployee.getId()));
            contactInfoService.saveOrUpdateEmployeeContactInfo(employeeDto.getEmployeeContactInfoList());
        }
        return savedEmployee;
    }

    public void updateEmployee(Employee employee) {
        Employee entity = findEmployee(employee.getId());
        if (entity != null) {
            entity.setName(employee.getName());
            entity.setSurname(employee.getSurname());
            entity.setDepartId(employee.getDepartId());
            entity.setPosition(employee.getPosition());
            entity.setRegion(employee.getRegion());
            entity.setActive(employee.getActive());
            entity.setUserId(employee.getUserId());
        }
    }

    public void updateEmployee(EmployeeDto employeeDto) {
        Employee employee = EmployeeMapper.INSTANCE.employeeDtoToEmployee(employeeDto);
        updateEmployee(employee);
        contactInfoService.saveOrUpdateEmployeeContactInfo(employeeDto.getEmployeeContactInfoList());
    }

    public void deleteEmployee(Integer employeeId) {
        Integer userId = findEmployee(employeeId).getUserId();
        if (userId != null) {
            userService.deleteUser(userId);
        }
        contactInfoService.deleteEmployeeContactInfo(employeeId);
        dao.deleteEmployee(employeeId);
    }
}
