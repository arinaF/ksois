package rtu.master.work.service;

import rtu.master.work.entity.Invoice;

import java.util.List;

public interface InvoiceService {

    Invoice findInvoice(Integer id);

    Invoice findInvoice(String identifier);

    List<Invoice> findAllInvoices();

    void updateOrSaveInvoice(Invoice invoice);

    void deleteInvoice(Integer id);
}
