package rtu.master.work.service;

import rtu.master.work.entity.EmployeeContactInfo;

import java.util.List;

public interface ContactInfoService {

    EmployeeContactInfo findEmployeeContactInfoById(Integer id);

    List<EmployeeContactInfo> findEmployeeContactInfo(Integer employeeId);

    void saveOrUpdateEmployeeContactInfo(List<EmployeeContactInfo> employeeContactInfoList);

    void deleteEmployeeContactInfoById(Integer id);

    void deleteEmployeeContactInfo(Integer employeeId);
}
