package rtu.master.work.service;

import rtu.master.work.entity.Department;

import java.util.List;

public interface DepartmentService {

    Department findDepartment(Integer id);

    List<Department> findAllDepartments();

    List<Department> findAllDepartments(String type, String code);
}
