package rtu.master.work.service;

import rtu.master.work.entity.Classifier;

import java.util.List;

public interface ClassifierService {

    List<Classifier> findAllClassifiers(String parentCode);

    List<Classifier> findAllClassifiers(String parentCode, String type);

    Classifier findClassifier(String code);
}
