package rtu.master.work.service;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.dto.ProblemDto;
import rtu.master.work.entity.EmployeeContactInfo;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

@Service("emailService")
@Transactional
@PropertySources({
        @PropertySource("classpath:internal.properties")
})
public class EmailServiceImpl implements EmailService {

    @Autowired
    private ContactInfoService contactInfoService;

    @Autowired
    private Environment environment;

    @Override
    public void sendEmail(ProblemDto problem) {
        if (problem.getManagerId() != null) {
            String contactInfo =
                    contactInfoService.findEmployeeContactInfo(problem.getManagerId())
                            .stream().filter(c -> "EMAIL".equals(c.getType()))
                            .map(EmployeeContactInfo::getContact)
                            .findFirst().get();
            if (StringUtils.isNotBlank(contactInfo)) {
                try {
                    final String senderEmail = environment.getRequiredProperty("email.sender.address");
                    final String password = environment.getRequiredProperty("email.sender.password");
                    final String recipientEmail = contactInfo;

                    Properties props = new Properties();
                    props.put("mail.smtp.host", environment.getRequiredProperty("email.server.host"));
                    props.put("mail.smtp.port", environment.getRequiredProperty("email.server.port"));
                    props.put("mail.smtp.auth", "true");
                    props.put("mail.smtp.starttls.enable", "true");

                    Authenticator auth = new Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(senderEmail, password);
                        }
                    };
                    Session session = Session.getInstance(props, auth);

                    MimeMessage message = new MimeMessage(session);
                    message.setFrom(new InternetAddress(senderEmail));
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(recipientEmail));

                    message.setSubject("Informācija par pieteikto problēmu");
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

                    String formattedMessage = "Reģistrācijas datums: " + dateFormat.format(new Date()) + "<br>" +
                            "PPR numurs: " + problem.getInvoiceIdentifier() + "<br>" +
                            "PPR datums: " + dateFormat.format(problem.getInvoiceDate()) + "<br>" +
                            "Klienta POS nr.: " + problem.getClientPosNr() + "<br>" +
                            "Klienta nosaukums: " + problem.getClientName() + "<br>" +
                            "Klienta adrese: " + problem.getClientAddress() + "<br>" +
                            "Problemas apraksts: " + problem.getDescription() + "<br>" +
                            "Projekts: " + problem.getProjectCode() + "<br>";
                    message.setContent(formattedMessage, "text/html; charset=utf-8");

                    Transport.send(message);
                    System.out.println("Mail Sent");
                } catch (Exception ex) {
                    System.out.println("Mail fail");
                    System.out.println(ex);
                }
            }
        }
    }
}
