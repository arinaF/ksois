package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import rtu.master.work.dao.ContactInfoDao;
import rtu.master.work.entity.EmployeeContactInfo;

import java.util.List;
import java.util.stream.Collectors;

@Service("contactInfoService")
@Transactional
public class ContactInfoServiceImpl implements ContactInfoService {

    @Autowired
    private ContactInfoDao dao;

    @Override
    public EmployeeContactInfo findEmployeeContactInfoById(Integer id) {
        return dao.findEmployeeContactInfoById(id);
    }

    @Override
    public List<EmployeeContactInfo> findEmployeeContactInfo(Integer employeeId) {
        return dao.findEmployeeContactInfo(employeeId);
    }

    private void saveContactInfo(EmployeeContactInfo employeeContactInfo) {
        dao.saveEmployeeContactInfo(employeeContactInfo);
    }

    private void updateContactInfo(EmployeeContactInfo employeeContactInfo) {
        EmployeeContactInfo entity = findEmployeeContactInfoById(employeeContactInfo.getId());
        if (entity != null) {
            entity.setEmployeeId(employeeContactInfo.getEmployeeId());
            entity.setType(employeeContactInfo.getType());
            entity.setContact(employeeContactInfo.getContact());
            entity.setActive(employeeContactInfo.getActive());
        }
    }

    @Override
    public void saveOrUpdateEmployeeContactInfo(List<EmployeeContactInfo> employeeContactInfoList) {
        if (!CollectionUtils.isEmpty(employeeContactInfoList)) {
            deleteUnnecessaryContactInfo(employeeContactInfoList);
            employeeContactInfoList.forEach(c -> {
                if (c.getId() == null) {
                    saveContactInfo(c);
                } else {
                    updateContactInfo(c);
                }
            });
        }
    }

    @Override
    public void deleteEmployeeContactInfoById(Integer id) {
        dao.deleteEmployeeContactInfoById(id);
    }

    @Override
    public void deleteEmployeeContactInfo(Integer employeeId) {
        dao.deleteEmployeeContactInfo(employeeId);
    }

    private void deleteUnnecessaryContactInfo(List<EmployeeContactInfo> employeeContactInfoList) {
        List<Integer> contactsIdsFromDB =
                findEmployeeContactInfo(employeeContactInfoList.get(0).getEmployeeId())
                        .stream()
                        .map(EmployeeContactInfo::getId)
                        .collect(Collectors.toList());

        List<Integer> givenContacts = employeeContactInfoList
                .stream()
                .map(EmployeeContactInfo::getId)
                .collect(Collectors.toList());

        contactsIdsFromDB.forEach(cDB -> {
            if (!givenContacts.contains(cDB)) {
                deleteEmployeeContactInfoById(cDB);
            }
        });
    }
}
