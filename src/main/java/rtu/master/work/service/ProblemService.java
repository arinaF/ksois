package rtu.master.work.service;

import rtu.master.work.dto.ProblemDto;
import rtu.master.work.dto.ProblemShortDto;
import rtu.master.work.entity.Problem;

import java.util.List;
import java.util.Map;

public interface ProblemService {

    List<Problem> findAllProblems();

    List<Problem> findAllOperatorProblems(Integer operatorId);

    List<ProblemShortDto> findAllDriverProblems(Integer driverId);

    List<ProblemShortDto> findAllClientProblems(Integer clientId);

    Problem findProblem(Integer id);

    ProblemDto findFullProblem(Integer id);

    ProblemShortDto findFullProblemShort(Integer id);

    List<ProblemShortDto> findAllFullProblems();

    List<ProblemShortDto> findAllOperatorFullProblems(Integer operatorId);

    Problem saveProblem(ProblemDto problemDto);

    void updateProblem(Problem problem);

    void updateProblem(ProblemDto problemDto);

    void deleteProblem(Integer id);

    Map<String, Object> getProblemDataByInvoice(String invoiceIdentifier);

    Map<String, Object> getProblemDataByPosNr(String posNr);

}
