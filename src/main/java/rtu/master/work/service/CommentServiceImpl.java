package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.dao.CommentDao;
import rtu.master.work.dto.CommentDto;
import rtu.master.work.entity.Comment;
import rtu.master.work.entity.Employee;
import rtu.master.work.mappers.CommentMapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service("commentService")
@Transactional
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentDao commentDao;

    @Autowired
    ClassifierService classifierService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    DepartmentService departmentService;

    @Autowired
    UserService userService;

    @Override
    public List<CommentDto> findAllComments(Integer problemId) {
        List<Comment> comments = commentDao.findAllComments(problemId);
        List<CommentDto> fullComments = new ArrayList<>();

        comments.forEach(c -> {
            CommentDto full = CommentMapper.INSTANCE.commentToCommentDto(c);
            Employee employee = employeeService.findEmployee(c.getEmplId());
            String position = classifierService.findClassifier(employee.getPosition()).getName();
            String department = departmentService.findDepartment(employee.getDepartId()).getName();

            full.setAuthorName(employee.getName());
            full.setAuthorSurname(employee.getSurname());
            full.setAuthorPosition(position);
            full.setAuthorDepartment(department);
            fullComments.add(full);
        });

        return fullComments;
    }

    @Override
    public Comment findComment(Integer id) {
        return commentDao.findComment(id);
    }

    @Override
    public void saveOrUpdateComment(CommentDto commentDto, String username) {
        if (commentDto != null) {
            if (commentDto.getId() == null) {
                saveComment(commentDto, username);
            } else {
                updateComment(commentDto);
            }
        }
    }

    @Override
    public void deleteComment(Integer id) {
        commentDao.deleteComment(id);
    }

    private void updateComment(CommentDto commentDto) {
        Comment entity = findComment(commentDto.getId());
        if (entity != null) {
            entity.setComText(commentDto.getComText());
            entity.setDate(new Date());
        }
    }

    private Comment saveComment(CommentDto commentDto, String username) {
        Comment comment = CommentMapper.INSTANCE.commentDtoToComment(commentDto);
        Integer userId = userService.findUserByUserName(username).getId();
        Integer emplId = employeeService.findByUserId(userId).getId();

        comment.setDate(new Date());
        comment.setUserId(userId);
        comment.setComText(commentDto.getComText());
        comment.setEmplId(emplId);

        return commentDao.saveComment(comment);
    }
}
