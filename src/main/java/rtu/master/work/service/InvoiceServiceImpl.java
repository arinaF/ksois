package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.dao.InvoiceDao;
import rtu.master.work.entity.Invoice;

import java.util.List;

@Service("invoiceService")
@Transactional
public class InvoiceServiceImpl implements InvoiceService {

    @Autowired
    InvoiceDao dao;

    @Override
    public Invoice findInvoice(Integer id) {
        return dao.findInvoice(id);
    }

    @Override
    public Invoice findInvoice(String identifier) {
        return dao.findInvoice(identifier);
    }

    @Override
    public List<Invoice> findAllInvoices() {
        return dao.findAllInvoices();
    }

    @Override
    public void updateOrSaveInvoice(Invoice invoice) {
        if (invoice != null) {
            if (invoice.getId() == null) {
                saveInvoice(invoice);
            } else {
                updateInvoice(invoice);
            }
        }
    }

    @Override
    public void deleteInvoice(Integer id) {
        dao.deleteInvoice(id);
    }

    public void saveInvoice(Invoice invoice) {
        dao.saveInvoice(invoice);
    }

    public void updateInvoice(Invoice invoice) {
        Invoice entity = findInvoice(invoice.getId());
        if (entity != null) {
            entity.setIdentifier(invoice.getIdentifier());
            entity.setDate(invoice.getDate());
            entity.setClientId(invoice.getClientId());
            entity.setProjectId(invoice.getProjectId());
            entity.setWarehouseId(invoice.getWarehouseId());
        }
    }
}
