package rtu.master.work.service;

import rtu.master.work.dto.ClientDto;
import rtu.master.work.entity.Client;

import java.util.List;

public interface ClientService {

    List<Client> findAllClients();

    Client findClient(Integer id);

    Client findByPosNr(Integer posNr);

    ClientDto findFullClient(Integer id);

    ClientDto findFullClient(Client client);

    List<ClientDto> findFullClients();

    Client saveClient(ClientDto clientDto);

    void updateClient(Client client);

    void updateClient(ClientDto clientDto);

    void deleteClient(Integer id);
}
