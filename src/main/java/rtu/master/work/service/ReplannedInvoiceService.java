package rtu.master.work.service;

import rtu.master.work.dto.ReplannedInvoiceDto;
import rtu.master.work.dto.RouteInvoiceDto;

import java.util.List;

public interface ReplannedInvoiceService {

    List<ReplannedInvoiceDto> findAllReplannedInvoices();

    void updateReplannedInvoice(ReplannedInvoiceDto replannedInvoiceDto);

    void saveReplannedInvoices(List<RouteInvoiceDto> routeInvoices, String username);

    void deleteReplannedInvoice(Integer id);
}
