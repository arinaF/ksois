package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import rtu.master.work.dao.ClientContactInfoDao;
import rtu.master.work.entity.ClientContactInfo;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service("clientContactInfoService")
@Transactional
public class ClientContactInfoServiceImpl implements ClientContactInfoService {

    @Autowired
    private ClientContactInfoDao dao;

    @Override
    public ClientContactInfo findClientContactInfoById(Integer id) {
        return dao.findClientContactInfoById(id);
    }

    @Override
    public List<ClientContactInfo> findClientContactInfo(Integer clientId) {
        return dao.findClientContactInfo(clientId);
    }

    @Override
    public void saveOrUpdateClientContactInfo(List<ClientContactInfo> clientContactInfoList) {
        if (!CollectionUtils.isEmpty(clientContactInfoList)) {
            deleteUnnecessaryClientContactInfo(clientContactInfoList);
            clientContactInfoList.forEach(c -> {
                if (c.getId() == null) {
                    saveClientContactInfo(c);
                } else {
                    updateClientContactInfo(c);
                }
            });
        }
    }

    @Override
    public void deleteClientContactInfoById(Integer id) {
        dao.deleteClientContactInfoById(id);
    }

    @Override
    public void deleteClientContactInfo(Integer clientId) {
        dao.deleteClientContactInfo(clientId);
    }

    private void deleteUnnecessaryClientContactInfo(List<ClientContactInfo> clientContactInfoList) {
        List<Integer> contactIdsFromDB =
                findClientContactInfo(clientContactInfoList.get(0).getClientId())
                        .stream()
                        .map(ClientContactInfo::getId)
                        .collect(Collectors.toList());

        List<Integer> givenContacts = clientContactInfoList
                .stream()
                .map(ClientContactInfo::getId)
                .collect(Collectors.toList());

        contactIdsFromDB.forEach(cDB -> {
            if (!givenContacts.contains(cDB)) {
                deleteClientContactInfoById(cDB);
            }
        });
    }

    private void saveClientContactInfo(ClientContactInfo clientContactInfo) {
        dao.saveClientContactInfo(clientContactInfo);
    }

    private void updateClientContactInfo(ClientContactInfo clientContactInfo) {
        ClientContactInfo entity = findClientContactInfoById(clientContactInfo.getId());
        if (entity != null) {
            entity.setClientId(clientContactInfo.getClientId());
            entity.setType(clientContactInfo.getType());
            entity.setContact(clientContactInfo.getContact());
            entity.setContactPerson(clientContactInfo.getContactPerson());
            entity.setActive(clientContactInfo.getActive());
        }
    }
}
