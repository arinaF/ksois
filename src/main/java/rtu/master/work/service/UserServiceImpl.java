package rtu.master.work.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.dao.UserInfoDao;
import rtu.master.work.dto.EmployeeDto;
import rtu.master.work.dto.UserDto;
import rtu.master.work.entity.Employee;
import rtu.master.work.entity.UserInfo;
import rtu.master.work.mappers.EmployeeMapper;
import rtu.master.work.mappers.UserMapper;

import java.util.ArrayList;
import java.util.List;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserInfoDao userInfoDao;

    @Autowired
    private EmployeeService employeeService;

    @Override
    public UserInfo findUserByUserName(String username) {
        List<UserInfo> users = userInfoDao.findUserByUserName(username);

        if (users.size() > 0) {
            return users.get(0);
        } else {
            return null;
        }
    }

    @Override
    public List<UserDto> findAllUsers() {
        List<UserDto> detailedUsers = new ArrayList<>();
        List<UserInfo> users = userInfoDao.findAllUsers();
        users.forEach(u -> {
            EmployeeDto employee = employeeService.findByUserId(u.getId());
            if (employee != null && employee.getId() != null) {
                UserDto dto = new UserDto();
                dto.setUserId(u.getId());
                dto.setEmployeeId(employee.getId());
                dto.setUsername(u.getUsername());
                dto.setRole(u.getRole());
                dto.setName(employee.getName());
                dto.setSurname(employee.getSurname());
                dto.setDepartmentId(employee.getDepartment().getId());
                dto.setDepartment(employee.getDepartment().getName());
                dto.setPositionCode(employee.getPosition().getCode());
                dto.setPositionName(employee.getPosition().getName());
                dto.setRegionCode(employee.getRegion().getCode());
                dto.setRegionName(employee.getRegion().getName());
                detailedUsers.add(dto);
            }
        });

        return detailedUsers;
    }

    @Override
    public UserInfo findUser(Integer id) {
        return userInfoDao.findUser(id);
    }

    @Override
    public UserInfo saveUser(UserDto userDto) {
        Employee employee = employeeService.findEmployee(userDto.getEmployeeId());
        userDto.setPassword(BCrypt.hashpw(userDto.getPassword(), BCrypt.gensalt()));
        UserInfo savedUser = userInfoDao.saveUser(UserMapper.INSTANCE.userDtoToUserInfo(userDto));
        if (savedUser.getId() != null) {
            employee.setUserId(savedUser.getId());
            employeeService.updateEmployee(employee);
            return savedUser;
        }
        return null;
    }

    @Override
    public void updateUserInfo(UserDto userDto) {
        UserInfo entity = findUser(userDto.getUserId());
        if (entity != null) {
            String hashedPassword = BCrypt.hashpw(userDto.getPassword(), BCrypt.gensalt());
            entity.setUsername(userDto.getUsername());
            entity.setPassword(hashedPassword);
            entity.setRole(userDto.getRole());
        }
    }

    @Override
    public void deleteUserAndUpdateEmployee(Integer userId) {
        EmployeeDto employeeDto = employeeService.findByUserId(userId);
        Employee employee = EmployeeMapper.INSTANCE.employeeDtoToEmployee(employeeDto);
        employee.setUserId(null);
        employeeService.updateEmployee(employee);
        deleteUser(userId);
    }

    @Override
    public void deleteUser(Integer id) {
        userInfoDao.deleteUser(id);
    }
}
