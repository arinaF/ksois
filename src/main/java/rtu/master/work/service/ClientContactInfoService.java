package rtu.master.work.service;

import rtu.master.work.entity.ClientContactInfo;

import java.util.List;

public interface ClientContactInfoService {

    ClientContactInfo findClientContactInfoById(Integer id);

    List<ClientContactInfo> findClientContactInfo(Integer clientId);

    void saveOrUpdateClientContactInfo(List<ClientContactInfo> clientContactInfoList);

    void deleteClientContactInfoById(Integer id);

    void deleteClientContactInfo(Integer clientId);
}
