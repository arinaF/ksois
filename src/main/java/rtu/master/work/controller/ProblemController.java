package rtu.master.work.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import rtu.master.work.dto.ProblemDto;
import rtu.master.work.dto.ProblemShortDto;
import rtu.master.work.entity.UserInfo;
import rtu.master.work.exception.ValidationException;
import rtu.master.work.service.EmployeeService;
import rtu.master.work.service.ProblemService;
import rtu.master.work.service.UserService;
import rtu.master.work.validator.ProblemDtoValidator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class ProblemController {

    @Autowired
    ProblemService problemService;

    @Autowired
    UserService userService;

    @Autowired
    EmployeeService employeeService;

    @Autowired
    ProblemDtoValidator problemDtoValidator;

    @RequestMapping(value = "/problems/", method = RequestMethod.GET)
    public ResponseEntity<List<ProblemShortDto>> listAllProblems(
            @RequestParam(value = "operatorOnly", required = true) Boolean operatorOnly) {
        List<ProblemShortDto> problems;
        if (operatorOnly) {
            User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            UserInfo user = userService.findUserByUserName(principal.getUsername());
            Integer operatorId = employeeService.findByUserId(user.getId()).getId();
            problems = problemService.findAllOperatorFullProblems(operatorId);
        } else {
            problems = problemService.findAllFullProblems();
        }
        return new ResponseEntity<>(problems, HttpStatus.OK);
    }

    @RequestMapping(value = "problems/driver/{driverId}", method = RequestMethod.GET)
    public ResponseEntity<List<ProblemShortDto>> listAllDriverProblems(
            @PathVariable("driverId") Integer driverId) {
        List<ProblemShortDto> problems = problemService.findAllDriverProblems(driverId);
        return new ResponseEntity<>(problems, HttpStatus.OK);
    }

    @RequestMapping(value = "problems/client/list/{clientId}", method = RequestMethod.GET)
    public ResponseEntity<List<ProblemShortDto>> listAllClientProblems(
            @PathVariable("clientId") Integer clientId) {
        List<ProblemShortDto> problems = problemService.findAllClientProblems(clientId);
        return new ResponseEntity<>(problems, HttpStatus.OK);
    }

    @RequestMapping(value = "/problems/{id}", method = RequestMethod.GET)
    public ResponseEntity<ProblemDto> getFullProblem(@PathVariable("id") Integer id) {
        ProblemDto problem = problemService.findFullProblem(id);
        return new ResponseEntity<>(problem, HttpStatus.OK);
    }

    @RequestMapping(value = "/problems/ppr/{invoice}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getDataByInvoice(@PathVariable("invoice") String invoiceIdentifier) {
        Map<String, Object> map = new HashMap<>();
        if (StringUtils.isNotBlank(invoiceIdentifier) && StringUtils.length(invoiceIdentifier) > 9) {
            map = problemService.getProblemDataByInvoice(invoiceIdentifier);
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @RequestMapping(value = "/problems/client/{posNr}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Object>> getDataByPosNr(@PathVariable("posNr") String posNr) {
        Map<String, Object> map = new HashMap<>();
        if (StringUtils.isNotBlank(posNr) && StringUtils.length(posNr) > 6) {
            map = problemService.getProblemDataByPosNr(posNr);
        }
        return new ResponseEntity<>(map, HttpStatus.OK);
    }

    @RequestMapping(value = "/problems/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ProblemDto> updateProblem(
            @PathVariable("id") Integer id,
            @RequestBody ProblemDto problemDto, BindingResult result) {

        validate(problemDto, result);
        problemService.updateProblem(problemDto);
        return new ResponseEntity<>(problemDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/problems/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteProblem(@PathVariable("id") Integer id) {
        problemService.deleteProblem(id);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "problems", method = RequestMethod.POST)
    public ResponseEntity<Void> addProblem(@RequestBody List<ProblemDto> problemDtoList, BindingResult result) {
        problemDtoList.forEach(pd -> validate(pd, result));
        if (!CollectionUtils.isEmpty(problemDtoList)) {
            problemDtoList.forEach(p -> problemService.saveProblem(p));

        }

        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    private void validate(ProblemDto dto, BindingResult result) {
        problemDtoValidator.validate(dto, result);
        if (result.hasErrors()) {
            throw new ValidationException(result.getAllErrors().stream().map(ObjectError::getCode).collect(Collectors.toList()));
        }
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity handleValidationException(ValidationException ex) {
        Map<String, List<String>> errors = new HashMap<>();
        errors.put("errors", ex.getMessages());
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
