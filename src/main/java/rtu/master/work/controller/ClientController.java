package rtu.master.work.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import rtu.master.work.dto.ClientDto;
import rtu.master.work.entity.Client;
import rtu.master.work.exception.ValidationException;
import rtu.master.work.service.ClientService;
import rtu.master.work.validator.ClientDtoValidator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class ClientController {

    @Autowired
    ClientService clientService;

    @Autowired
    ClientDtoValidator clientDtoValidator;

    @RequestMapping(value = "/clients/", method = RequestMethod.GET)
    public ResponseEntity<List<ClientDto>> listAllClients() {
        List<ClientDto> clients = clientService.findFullClients();
        return new ResponseEntity<>(clients, HttpStatus.OK);
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.GET)
    public ResponseEntity<ClientDto> getClient(@PathVariable("id") Integer id) {
        ClientDto client = clientService.findFullClient(id);
        if (client.getId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(client, HttpStatus.OK);
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.PUT)
    public ResponseEntity<ClientDto> updateClient(
            @PathVariable("id") Integer id,
            @RequestBody ClientDto clientDto, BindingResult result) {
        validate(clientDto, result, new HashMap<>());
        clientService.updateClient(clientDto);
        return new ResponseEntity<>(clientDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/clients/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteClient(@PathVariable("id") Integer id) {
        Map<String, Object> context = new HashMap<>();
        context.put("DELETE", null);
        ClientDto dto = new ClientDto();
        dto.setId(id);
        validate(dto, null, context);
        clientService.deleteClient(id);
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @RequestMapping(value = "/clients/", method = RequestMethod.POST)
    public ResponseEntity<Integer> addClient(@RequestBody ClientDto clientDto, BindingResult result) {
        validate(clientDto, result, new HashMap<>());
        Client createdClient = clientService.saveClient(clientDto);
        if (createdClient == null || createdClient.getId() == null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(createdClient.getId(), HttpStatus.CREATED);
    }

    private void validate(ClientDto dto, BindingResult result, Map<String, Object> map) {
        if(map.containsKey("DELETE")){
            clientDtoValidator.validateOnDelete(dto);
        } else {
            clientDtoValidator.validate(dto, result);
        }
        if (result.hasErrors()) {
            throw new ValidationException(result.getAllErrors().stream().map(ObjectError::getCode).collect(Collectors.toList()));
        }
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity handleValidationException(ValidationException ex) {
        Map<String, List<String>> errors = new HashMap<>();
        errors.put("errors", ex.getMessages());
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
