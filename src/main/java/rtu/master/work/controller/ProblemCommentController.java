package rtu.master.work.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import rtu.master.work.dto.CommentDto;
import rtu.master.work.exception.ValidationException;
import rtu.master.work.service.CommentService;

import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class ProblemCommentController {

    @Autowired
    CommentService commentService;

    @RequestMapping(value = "/comments/{problemId}", method = RequestMethod.GET)
    public ResponseEntity<List<CommentDto>> listAllComments(@PathVariable("problemId") Integer problemId) {
        List<CommentDto> comments = commentService.findAllComments(problemId);
        return new ResponseEntity<>(comments, HttpStatus.OK);
    }

    @RequestMapping(value = "/comments/", method = RequestMethod.POST)
    public ResponseEntity<Void> addComment(@RequestBody CommentDto commentDto,
                                           Principal principal,
                                           BindingResult result) {
        validate(commentDto, result);
        commentService.saveOrUpdateComment(commentDto, principal.getName());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/comments/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteComment(@PathVariable("id") Integer id) {
        commentService.deleteComment(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private void validate(CommentDto commentDto, BindingResult result) {
        if (commentDto != null && StringUtils.isBlank(commentDto.getComText())) {
            result.reject("Pievienojiet komentāra tekstu");
        }
        if (result.hasErrors()) {
            throw new ValidationException(result.getAllErrors().stream().map(ObjectError::getCode).collect(Collectors.toList()));
        }
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity handleValidationException(ValidationException ex) {
        Map<String, List<String>> errors = new HashMap<>();
        errors.put("errors", ex.getMessages());
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
