package rtu.master.work.controller;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rtu.master.work.entity.Classifier;
import rtu.master.work.service.ClassifierService;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ClassifierController {

    @Autowired
    ClassifierService service;

    @RequestMapping(value = "/classifiers/", method = RequestMethod.GET)
    public ResponseEntity<List<Classifier>> listClassifiers(
            @RequestParam(value = "parentCode") String parentCode,
            @RequestParam(value = "type", required = false) String type) {

        List<Classifier> classifiers = new ArrayList<>();
        if (StringUtils.isNotBlank(type)) {
            classifiers = service.findAllClassifiers(parentCode, type);
        } else {
            classifiers = service.findAllClassifiers(parentCode);
        }
        if (CollectionUtils.isEmpty(classifiers)) {
            return new ResponseEntity<List<Classifier>>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<List<Classifier>>(classifiers, HttpStatus.OK);
    }
}
