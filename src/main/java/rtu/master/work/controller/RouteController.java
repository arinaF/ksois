package rtu.master.work.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import rtu.master.work.dto.RouteDto;
import rtu.master.work.dto.RouteInvoiceDto;
import rtu.master.work.service.RouteInvoiceService;
import rtu.master.work.service.RouteService;

import java.util.List;

@RestController
public class RouteController {

    @Autowired
    RouteService routeService;

    @Autowired
    RouteInvoiceService routeInvoiceService;

    @RequestMapping(value = "/routes/", method = RequestMethod.GET)
    public ResponseEntity<List<RouteDto>> listAllRoutes() {
        List<RouteDto> routes = routeService.findAllRoutes();
        return new ResponseEntity<>(routes, HttpStatus.OK);
    }

    @RequestMapping(value = "/routes/{id}", method = RequestMethod.GET)
    public ResponseEntity<RouteDto> getRoute(@PathVariable("id") Integer id) {
        RouteDto route = routeService.findFullRoute(id);
        return new ResponseEntity<>(route, HttpStatus.OK);
    }

    @RequestMapping(value = "/routes/{id}/invoices", method = RequestMethod.GET)
    public ResponseEntity<List<RouteInvoiceDto>> listAllRouteInvoices(@PathVariable("id") Integer id) {
        List<RouteInvoiceDto> routeInvoices = routeInvoiceService.findRouteInvoices(id);
        return new ResponseEntity<>(routeInvoices, HttpStatus.OK);
    }

    @RequestMapping(value = "/routes/currentDay", method = RequestMethod.GET)
    public ResponseEntity<List<RouteDto>> listAllCurrentDayRoutes() {
        List<RouteDto> routes = routeService.findAllCurrentDayRoutes();
        return new ResponseEntity<>(routes, HttpStatus.OK);
    }

    @RequestMapping(value = "/routes/", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateRoute(@RequestBody RouteDto routeDto, BindingResult result) {
        routeService.updateRoute(routeDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
