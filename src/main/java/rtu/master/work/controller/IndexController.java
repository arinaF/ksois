package rtu.master.work.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rtu.master.work.entity.Employee;
import rtu.master.work.entity.UserInfo;
import rtu.master.work.service.EmployeeService;
import rtu.master.work.service.UserService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
public class IndexController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    UserService userService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getIndexPage() {
        return "";
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String loginPage() {
        return "login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "";
    }

    @RequestMapping(value = "/username", method = RequestMethod.GET)
    public ResponseEntity<UserInfo> getUserName(Principal principal) {
        UserInfo user = userService.findUserByUserName(principal.getName());
        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @RequestMapping(value = "/permissions", method = RequestMethod.GET)
    public ResponseEntity<List<String>> fetchPermissions() {
        List<String> permissions = SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getAuthorities()
                .stream()
                .map(role -> role.getAuthority()).collect(Collectors.toList());
        return new ResponseEntity<List<String>>(permissions, HttpStatus.OK);
    }

    @RequestMapping(value = "/permissions/edit/{id}", method = RequestMethod.GET)
    public ResponseEntity<Map<String, Boolean>> fetchEditPermissions(@PathVariable("id") Integer operatorId, Principal principal) {
        Employee operator = employeeService.findEmployee(operatorId);
        Map<String, Boolean> map = new HashMap<>();
        if (operator != null && operator.getUserId() != null) {
            UserInfo user = userService.findUserByUserName(principal.getName());
            Boolean hasEditPermission = operator.getUserId().equals(user.getId());
            map.put("hasEditPermission", hasEditPermission);
            return new ResponseEntity<>(map, HttpStatus.OK);
        }
        map.put("hasEditPermission", false);
        return new ResponseEntity<>(map, HttpStatus.OK);
    }
}
