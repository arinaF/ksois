package rtu.master.work.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import rtu.master.work.dto.EmployeeDto;
import rtu.master.work.entity.Employee;
import rtu.master.work.exception.ValidationException;
import rtu.master.work.service.EmployeeService;
import rtu.master.work.validator.EmployeeDtoValidator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class EmployeeController {

    @Autowired
    EmployeeService employeeService;

    @Autowired
    private EmployeeDtoValidator employeeDtoValidator;

    @RequestMapping(value = "/employees/", method = RequestMethod.GET)
    public ResponseEntity<List<EmployeeDto>> listAllEmployees(
            @RequestParam(value = "departType", required = false) String type,
            @RequestParam(value = "departCode", required = false) String code,
            @RequestParam(value = "departId", required = false) Integer id
    ) {
        List<EmployeeDto> employees = employeeService.findFullEmployees(type, code, id);
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }

    @RequestMapping(value = "/employees/{id}", method = RequestMethod.GET)
    public ResponseEntity<EmployeeDto> getEmployee(@PathVariable("id") Integer id) {
        EmployeeDto employee = employeeService.findFullEmployee(id);
        if (employee.getId() == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(employee, HttpStatus.OK);
    }

    @RequestMapping(value = "/employees/{id}", method = RequestMethod.PUT)
    public ResponseEntity<EmployeeDto> updateEmployee(
            @PathVariable("id") Integer id,
            @RequestBody EmployeeDto employeeDto, BindingResult result) {
        validate(employeeDto, result, new HashMap<>());
        employeeService.updateEmployee(employeeDto);
        return new ResponseEntity<>(employeeDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/employees/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteEmployee(@PathVariable("id") Integer id) {
        Map<String, Object> context = new HashMap<>();
        context.put("DELETE", null);
        EmployeeDto dto = new EmployeeDto();
        dto.setId(id);
        validate(dto, null, context);
        employeeService.deleteEmployee(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @RequestMapping(value = "/employees/", method = RequestMethod.POST)
    public ResponseEntity<Integer> addEmployee(@RequestBody EmployeeDto employeeDto, BindingResult result) {
        validate(employeeDto, result, new HashMap<>());
        Employee createdEmployee = employeeService.saveEmployee(employeeDto);
        if (createdEmployee == null || createdEmployee.getId() == null) {
            return new ResponseEntity<>(HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(createdEmployee.getId(), HttpStatus.CREATED);
    }

    private void validate(EmployeeDto dto, BindingResult result, Map<String, Object> context) {
        if (context.containsKey("DELETE")) {
            employeeDtoValidator.validateOnDelete(dto);
        } else {
            employeeDtoValidator.validate(dto, result);
        }
        if (result.hasErrors()) {
            throw new ValidationException(result.getAllErrors().stream().map(ObjectError::getCode).collect(Collectors.toList()));
        }
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity handleValidationException(ValidationException ex) {
        Map<String, List<String>> errors = new HashMap<>();
        errors.put("errors", ex.getMessages());
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
