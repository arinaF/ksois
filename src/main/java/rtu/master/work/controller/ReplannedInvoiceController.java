package rtu.master.work.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import rtu.master.work.dto.ReplannedInvoiceDto;
import rtu.master.work.dto.RouteInvoiceDto;
import rtu.master.work.service.ReplannedInvoiceService;

import java.security.Principal;
import java.util.List;

@RestController
public class ReplannedInvoiceController {

    @Autowired
    ReplannedInvoiceService replannedInvoiceService;

    @RequestMapping(value = "/replannedInvoices/", method = RequestMethod.GET)
    public ResponseEntity<List<ReplannedInvoiceDto>> listAllReplannedInvoices() {

        List<ReplannedInvoiceDto> replannedInvoices = replannedInvoiceService.findAllReplannedInvoices();
        return new ResponseEntity<>(replannedInvoices, HttpStatus.OK);
    }

    @RequestMapping(value = "/replannedInvoices/{id}", method = RequestMethod.PUT)
    public ResponseEntity<Void> updateReplannedInvoice(@PathVariable("id") Integer id, @RequestBody ReplannedInvoiceDto replannedInvoiceDto) {

        replannedInvoiceService.updateReplannedInvoice(replannedInvoiceDto);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/replannedInvoices/", method = RequestMethod.POST)
    public ResponseEntity<Void> addReplannedInvoices(@RequestBody List<RouteInvoiceDto> routeInvoices, Principal principal) {

        replannedInvoiceService.saveReplannedInvoices(routeInvoices, principal.getName());
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/replannedInvoices/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteReplannedInvoice(@PathVariable("id") Integer id) {

        replannedInvoiceService.deleteReplannedInvoice(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
