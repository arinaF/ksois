package rtu.master.work.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import rtu.master.work.dto.UserDto;
import rtu.master.work.exception.ValidationException;
import rtu.master.work.service.UserService;
import rtu.master.work.validator.UserDtoValidator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserDtoValidator userDtoValidator;

    @RequestMapping(value = "/users/", method = RequestMethod.GET)
    public ResponseEntity<List<UserDto>> listAllUsers() {
        List<UserDto> users = userService.findAllUsers();
        return new ResponseEntity<>(users, HttpStatus.OK);
    }

    @RequestMapping(value = "/users/", method = RequestMethod.PUT)
    public ResponseEntity<UserDto> updateUserInfo(
            @RequestBody UserDto userDto, BindingResult result) {
        validate(userDto, result);
        userService.updateUserInfo(userDto);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/users/", method = RequestMethod.POST)
    public ResponseEntity<UserDto> saveUserInfo(
            @RequestBody UserDto userDto, BindingResult result) {
        validate(userDto, result);
        userService.saveUser(userDto);
        return new ResponseEntity<>(userDto, HttpStatus.OK);
    }

    @RequestMapping(value = "/users/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteUser(@PathVariable("id") Integer id) {
        userService.deleteUserAndUpdateEmployee(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private void validate(UserDto userDto, BindingResult result) {
        userDtoValidator.validate(userDto, result);
        if (result.hasErrors()) {
            throw new ValidationException(result.getAllErrors().stream().map(ObjectError::getCode).collect(Collectors.toList()));
        }
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity handleValidationException(ValidationException ex) {
        Map<String, List<String>> errors = new HashMap<>();
        errors.put("errors", ex.getMessages());
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
