package rtu.master.work.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rtu.master.work.entity.Department;
import rtu.master.work.service.DepartmentService;

import java.util.List;

@RestController
public class DepartmentController {

    @Autowired
    DepartmentService service;

    @RequestMapping(value = "/departments/", method = RequestMethod.GET)
    public ResponseEntity<List<Department>> listAllDepartments() {

        List<Department> departments = service.findAllDepartments();
        if (CollectionUtils.isEmpty(departments)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(departments, HttpStatus.OK);
    }

    @RequestMapping(value = "/departments/codeAndType", method = RequestMethod.GET)
    public ResponseEntity<List<Department>> listDepartments(
            @RequestParam(value = "type", required = true) String type,
            @RequestParam(value = "code", required = false) String code) {

        List<Department> departments = service.findAllDepartments(type, code);
        if (CollectionUtils.isEmpty(departments)) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(departments, HttpStatus.OK);
    }
}
