package rtu.master.work.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import rtu.master.work.entity.Project;
import rtu.master.work.exception.ValidationException;
import rtu.master.work.service.ProjectService;
import rtu.master.work.validator.ProjectValidator;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@RestController
public class ProjectController {

    @Autowired
    ProjectService projectService;

    @Autowired
    ProjectValidator projectValidator;

    @RequestMapping(value = "/projects/", method = RequestMethod.GET)
    public ResponseEntity<List<Project>> listAllProjects() {
        List<Project> projects = projectService.findAllProjects();
        return new ResponseEntity<>(projects, HttpStatus.OK);
    }

    @RequestMapping(value = "/projects/{id}", method = RequestMethod.GET)
    public ResponseEntity<Project> getProject(@PathVariable("id") Integer id) {
        Project project = projectService.findProject(id);
        return new ResponseEntity<>(project, HttpStatus.OK);
    }

    @RequestMapping(value = "/projects/", method = RequestMethod.POST)
    public ResponseEntity<Void> addProject(@RequestBody Project project, BindingResult result) {
        validate(project, result);
        projectService.updateOrSaveProject(project);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @RequestMapping(value = "/projects/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Void> deleteProject(@PathVariable("id") Integer id) {
        projectService.deleteProject(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private void validate(Project project, BindingResult result) {
        if (project != null) {
            projectValidator.validate(project, result);
        }
        if (result.hasErrors()) {
            throw new ValidationException(result.getAllErrors().stream().map(ObjectError::getCode).collect(Collectors.toList()));
        }
    }

    @ExceptionHandler(ValidationException.class)
    public ResponseEntity handleValidationException(ValidationException ex) {
        Map<String, List<String>> errors = new HashMap<>();
        errors.put("errors", ex.getMessages());
        return new ResponseEntity<>(errors, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
