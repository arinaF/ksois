package rtu.master.work.dto;

import rtu.master.work.entity.Classifier;
import rtu.master.work.entity.Department;
import rtu.master.work.entity.EmployeeContactInfo;

import java.util.List;

public class EmployeeDto {

    private Integer id;
    private Department department;
    private Classifier position;
    private Classifier region;
    private String name;
    private String surname;
    private Boolean active;
    private Integer userId;
    private List<EmployeeContactInfo> employeeContactInfoList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Classifier getPosition() {
        return position;
    }

    public void setPosition(Classifier position) {
        this.position = position;
    }

    public Classifier getRegion() {
        return region;
    }

    public void setRegion(Classifier region) {
        this.region = region;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Boolean getActive() {
        return active;
    }

    public void setActive(Boolean active) {
        this.active = active;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public List<EmployeeContactInfo> getEmployeeContactInfoList() {
        return employeeContactInfoList;
    }

    public void setEmployeeContactInfoList(List<EmployeeContactInfo> employeeContactInfoList) {
        this.employeeContactInfoList = employeeContactInfoList;
    }
}
