package rtu.master.work.dto;

import rtu.master.work.entity.Classifier;
import rtu.master.work.entity.ClientContactInfo;

import java.util.List;

public class ClientDto {

    private Integer id;
    private Integer posNr;
    private String regNr;
    private String name;
    private String address;
    private Float lat;
    private Float lng;
    private Classifier region;
    private Boolean isActive;
    private List<ClientContactInfo> clientContactInfoList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getPosNr() {
        return posNr;
    }

    public void setPosNr(Integer posNr) {
        this.posNr = posNr;
    }

    public String getRegNr() {
        return regNr;
    }

    public void setRegNr(String regNr) {
        this.regNr = regNr;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Float getLat() {
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public Classifier getRegion() {
        return region;
    }

    public void setRegion(Classifier region) {
        this.region = region;
    }

    public Boolean getActive() {
        return isActive;
    }

    public void setActive(Boolean active) {
        isActive = active;
    }

    public List<ClientContactInfo> getClientContactInfoList() {
        return clientContactInfoList;
    }

    public void setClientContactInfoList(List<ClientContactInfo> clientContactInfoList) {
        this.clientContactInfoList = clientContactInfoList;
    }
}
