package rtu.master.work.dto;

import java.util.Date;

public class ReplannedInvoiceDto {

    private Integer id;
    private Integer invoiceId;
    private String comment;
    private Date regDate;
    private Date deliveryDate;
    private Integer operatorId;
    private Integer routeId;
    private Boolean replanned;

    private String invoiceIdentifier;
    private String operatorName;
    private String operatorSurname;
    private String routeCode;

    private Integer clientId;
    private String clientName;
    private String clientAddress;
    private Integer clientPosNr;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public Date getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(Date deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Boolean getReplanned() {
        return replanned;
    }

    public void setReplanned(Boolean replanned) {
        this.replanned = replanned;
    }

    public String getInvoiceIdentifier() {
        return invoiceIdentifier;
    }

    public void setInvoiceIdentifier(String invoiceIdentifier) {
        this.invoiceIdentifier = invoiceIdentifier;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getOperatorSurname() {
        return operatorSurname;
    }

    public void setOperatorSurname(String operatorSurname) {
        this.operatorSurname = operatorSurname;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public Integer getClientPosNr() {
        return clientPosNr;
    }

    public void setClientPosNr(Integer clientPosNr) {
        this.clientPosNr = clientPosNr;
    }
}
