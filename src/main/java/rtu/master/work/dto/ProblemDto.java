package rtu.master.work.dto;

import rtu.master.work.entity.ProblemComment;

import java.util.Date;
import java.util.List;

public class ProblemDto {

    private Integer id;
    private String identifier;
    private Date regDate;
    private String source;
    private String description;
    private String respDepartment;
    private String action;
    private String reason;
    private Boolean informed;
    private Date closeDate;

    private String sourceName;
    private String actionName;
    private String reasonName;

    private Integer invoiceId;
    private String invoiceIdentifier;
    private Date invoiceDate;

    private Integer clientId;
    private String clientName;
    private String clientAddress;
    private Integer clientPosNr;

    private Integer projectId;
    private String projectCode;

    private Integer warehouseId;
    private String warehouseName;

    private Integer operatorId;
    private String operatorName;
    private String operatorSurname;

    private Integer driverId;
    private String driverName;
    private String driverSurname;

    private Integer managerId;
    private String managerName;
    private String managerSurname;

    private Integer routeId;
    private String routeCode;

    private Integer backInvId;
    private String backInvIdent;

    private List<ProblemComment> problemComments;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public Date getRegDate() {
        return regDate;
    }

    public void setRegDate(Date regDate) {
        this.regDate = regDate;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRespDepartment() {
        return respDepartment;
    }

    public void setRespDepartment(String respDepartment) {
        this.respDepartment = respDepartment;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Boolean getInformed() {
        return informed;
    }

    public void setInformed(Boolean informed) {
        this.informed = informed;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }

    public String getActionName() {
        return actionName;
    }

    public void setActionName(String actionName) {
        this.actionName = actionName;
    }

    public String getReasonName() {
        return reasonName;
    }

    public void setReasonName(String reasonName) {
        this.reasonName = reasonName;
    }

    public Integer getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(Integer invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getInvoiceIdentifier() {
        return invoiceIdentifier;
    }

    public void setInvoiceIdentifier(String invoiceIdentifier) {
        this.invoiceIdentifier = invoiceIdentifier;
    }

    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Integer getClientId() {
        return clientId;
    }

    public void setClientId(Integer clientId) {
        this.clientId = clientId;
    }

    public String getClientName() {
        return clientName;
    }

    public void setClientName(String clientName) {
        this.clientName = clientName;
    }

    public String getClientAddress() {
        return clientAddress;
    }

    public void setClientAddress(String clientAddress) {
        this.clientAddress = clientAddress;
    }

    public Integer getClientPosNr() {
        return clientPosNr;
    }

    public void setClientPosNr(Integer clientPosNr) {
        this.clientPosNr = clientPosNr;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getProjectCode() {
        return projectCode;
    }

    public void setProjectCode(String projectCode) {
        this.projectCode = projectCode;
    }

    public Integer getWarehouseId() {
        return warehouseId;
    }

    public void setWarehouseId(Integer warehouseId) {
        this.warehouseId = warehouseId;
    }

    public String getWarehouseName() {
        return warehouseName;
    }

    public void setWarehouseName(String warehouseName) {
        this.warehouseName = warehouseName;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getOperatorSurname() {
        return operatorSurname;
    }

    public void setOperatorSurname(String operatorSurname) {
        this.operatorSurname = operatorSurname;
    }

    public Integer getDriverId() {
        return driverId;
    }

    public void setDriverId(Integer driverId) {
        this.driverId = driverId;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getDriverSurname() {
        return driverSurname;
    }

    public void setDriverSurname(String driverSurname) {
        this.driverSurname = driverSurname;
    }

    public Integer getManagerId() {
        return managerId;
    }

    public void setManagerId(Integer managerId) {
        this.managerId = managerId;
    }

    public String getManagerName() {
        return managerName;
    }

    public void setManagerName(String managerName) {
        this.managerName = managerName;
    }

    public String getManagerSurname() {
        return managerSurname;
    }

    public void setManagerSurname(String managerSurname) {
        this.managerSurname = managerSurname;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public String getRouteCode() {
        return routeCode;
    }

    public void setRouteCode(String routeCode) {
        this.routeCode = routeCode;
    }

    public Integer getBackInvId() {
        return backInvId;
    }

    public void setBackInvId(Integer backInvId) {
        this.backInvId = backInvId;
    }

    public String getBackInvIdent() {
        return backInvIdent;
    }

    public void setBackInvIdent(String backInvIdent) {
        this.backInvIdent = backInvIdent;
    }

    public List<ProblemComment> getProblemComments() {
        return problemComments;
    }

    public void setProblemComments(List<ProblemComment> problemComments) {
        this.problemComments = problemComments;
    }
}