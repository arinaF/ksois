package rtu.master.work.config.spring;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@Configuration
@EnableWebMvc
@ComponentScan("rtu.master.work")
@PropertySources({
        @PropertySource("classpath:internal.properties")
})
public class SpringConfiguration extends WebMvcConfigurerAdapter {

    @Autowired
    private Environment environment;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        if (environment.acceptsProfiles("production")) {
            registry.addResourceHandler("/**").addResourceLocations("/WEB-INF/dist/");
            registry.addResourceHandler("/directives/**").addResourceLocations("/WEB-INF/directives/");
            registry.addResourceHandler("/img/**").addResourceLocations("/WEB-INF/img/");
            registry.addResourceHandler("/vendor/**").addResourceLocations("/WEB-INF/vendor/");
        }else{
            registry.addResourceHandler("/**").addResourceLocations("/WEB-INF/");
        }
    }

    @Bean
    public InternalResourceViewResolver getInternalResourceViewResolver() {
        InternalResourceViewResolver resolver = new InternalResourceViewResolver();
        resolver.setPrefix("/WEB-INF/");
        resolver.setSuffix(".jsp");
        resolver.setRedirectHttp10Compatible(false);
        return resolver;
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
