package rtu.master.work.dao;

import rtu.master.work.entity.ReplannedInvoice;

import java.util.Date;
import java.util.List;

public interface ReplannedInvoiceDao {

    ReplannedInvoice findReplannedInvoice(Integer id);

    List<ReplannedInvoice> findAllReplannedInvoices();

    void saveReplannedInvoice(ReplannedInvoice replannedInvoice);

    void deleteReplannedInvoice(Integer id);

    int countReplannedInvoices(Integer invoiceId, Date deliveryDate, Integer routeId);
}
