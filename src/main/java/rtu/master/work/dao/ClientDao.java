package rtu.master.work.dao;

import rtu.master.work.entity.Client;

import java.util.List;

public interface ClientDao {

    List<Client> findAllClients();

    Client findClient(Integer id);

    Client findByPosNr(Integer posNr);

    Client saveClient(Client client);

    void deleteClient(Integer id);

    Client findClientByPosNr(Integer posNr);

    Client findClientByRegNr(String regNr);
}
