package rtu.master.work.dao;

import rtu.master.work.entity.Problem;

import java.util.List;

public interface ProblemDao {

    List<Problem> findAllProblems();

    List<Problem> findAllOperatorProblems(Integer operatorId);

    List<Problem> findAllDriverProblems(Integer driverId);

    List<Problem> findAllClientProblems(Integer clientId);

    Problem findProblem(Integer id);

    Problem saveProblem(Problem problem);

    void deleteProblem(Integer id);

    Integer currentDaysProblemCount(Integer operatorId);
}
