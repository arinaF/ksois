package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.entity.Comment;

import java.math.BigInteger;
import java.util.List;

@Repository("commentDao")
@Transactional
public class CommentDaoImpl extends AbstractDao<Integer, Comment> implements CommentDao {

    @Override
    public List<Comment> findAllComments(Integer problemId) {
        Criteria criteria = createEntityCriteria();
        return (List<Comment>) criteria
                .add(Restrictions.eq("problemId", problemId))
                .list();
    }

    @Override
    public Comment findComment(Integer id) {
        return getByKey(id);
    }

    @Override
    public Comment saveComment(Comment comment) {
        persist(comment);
        flush();
        return comment;
    }

    @Override
    public void deleteComment(Integer id) {
        Query query = getSession().createSQLQuery("delete from PROBLEMS_COMMENTS where ID = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

}
