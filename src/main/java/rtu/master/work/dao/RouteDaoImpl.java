package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.entity.Route;

import java.util.List;

@Repository("routeDao")
@Transactional
public class RouteDaoImpl extends AbstractDao<Integer, Route> implements RouteDao {

    @Override
    public Route findRoute(Integer id) {
        return getByKey(id);
    }

    @Override
    public List<Route> findAllRoutes() {
        Criteria criteria = createEntityCriteria();
        return (List<Route>) criteria.list();
    }

    @Override
    public List<Route> findAllCurrentDayRoutes() {
        Criteria criteria = createEntityCriteria();
        return (List<Route>) criteria
                .add(Restrictions.sqlRestriction("date_format(DATE, '%Y-%m-%d') = date_format(sysdate(), '%Y-%m-%d')"))
                .list();
    }

    @Override
    public List<Route> findDriverRoutes(Integer driverId) {
        Criteria criteria = createEntityCriteria();
        return (List<Route>) criteria
                .add(Restrictions.eq("driverId", driverId))
                .list();
    }
}
