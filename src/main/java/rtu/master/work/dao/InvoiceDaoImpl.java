package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.entity.Invoice;

import java.util.List;

@Repository("invoiceDao")
@Transactional
public class InvoiceDaoImpl extends AbstractDao<Integer, Invoice> implements InvoiceDao {

    @Override
    public Invoice findInvoice(Integer id) {
        if(id != null) {
            return getByKey(id);
        }
        return null;
    }

    @Override
    public Invoice findInvoice(String identifier) {
        Criteria criteria = createEntityCriteria();
        return (Invoice) criteria
                .add(Restrictions.eq("identifier", identifier))
                .uniqueResult();
    }

    @Override
    public List<Invoice> findAllInvoices() {
        Criteria criteria = createEntityCriteria();
        return (List<Invoice>) criteria.list();
    }

    @Override
    public Invoice saveInvoice(Invoice invoice) {
        persist(invoice);
        flush();
        return invoice;
    }

    @Override
    public void deleteInvoice(Integer id) {
        Query query = getSession().createSQLQuery("delete from INVOICES where ID =:id");
        query.setInteger("id", id);
        query.executeUpdate();
    }
}
