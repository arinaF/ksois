package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rtu.master.work.entity.UserInfo;

import javax.transaction.Transactional;
import java.util.List;

@Repository("userInfoDao")
@Transactional
public class UserInfoDaoImpl extends AbstractDao<Integer, UserInfo> implements UserInfoDao {

    @Override
    public List<UserInfo> findAllUsers() {
        Criteria criteria = createEntityCriteria();
        return (List<UserInfo>) criteria.list();
    }

    @Override
    public UserInfo findUser(Integer id) {
        Criteria criteria = createEntityCriteria();
        return (UserInfo) criteria
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    @Override
    public List<UserInfo> findUserByUserName(String username) {
        Criteria criteria = createEntityCriteria();
        return (List<UserInfo>) criteria
                .add(Restrictions.eq("username", username))
                .list();
    }

    @Override
    public UserInfo saveUser(UserInfo user) {
        persist(user);
        flush();
        return user;
    }

    @Override
    public void deleteUser(Integer id) {
        Query query = getSession().createSQLQuery("delete from USERS_ROLES where ID = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }
}
