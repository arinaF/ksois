package rtu.master.work.dao;

import rtu.master.work.entity.UserInfo;

import java.util.List;

public interface UserInfoDao {

    List<UserInfo> findAllUsers();

    UserInfo findUser(Integer id);

    List<UserInfo> findUserByUserName(String username);

    UserInfo saveUser(UserInfo user);

    void deleteUser(Integer id);
}
