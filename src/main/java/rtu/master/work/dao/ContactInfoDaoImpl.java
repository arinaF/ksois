package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rtu.master.work.entity.EmployeeContactInfo;

import javax.transaction.Transactional;
import java.util.List;

@Repository("contactInfoDao")
@Transactional
public class ContactInfoDaoImpl extends AbstractDao<Integer, EmployeeContactInfo> implements ContactInfoDao {

    @Override
    public EmployeeContactInfo findEmployeeContactInfoById(Integer id) {
        Criteria criteria = createEntityCriteria();
        return (EmployeeContactInfo) criteria
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<EmployeeContactInfo> findEmployeeContactInfo(Integer employeeId) {
        Criteria criteria = createEntityCriteria();
        return (List<EmployeeContactInfo>) criteria
                .add(Restrictions.eq("employeeId", employeeId))
                .list();
    }

    @Override
    public void saveEmployeeContactInfo(EmployeeContactInfo employeeContactInfo) {
        persist(employeeContactInfo);
    }

    @Override
    public void deleteEmployeeContactInfoById(Integer id) {
        Query query = getSession().createSQLQuery("delete from EMPLOYEES_CONTACT_INFO where ID = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    @Override
    public void deleteEmployeeContactInfo(Integer employeeId) {
        Query query = getSession().createSQLQuery("delete from EMPLOYEES_CONTACT_INFO where EMPL_ID = :employeeId");
        query.setInteger("employeeId", employeeId);
        query.executeUpdate();
    }
}
