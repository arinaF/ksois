package rtu.master.work.dao;

import rtu.master.work.entity.Employee;

import java.util.List;

public interface EmployeeDao {
    List<Employee> findAllEmployees();

    List<Employee> findAllEmployees(Integer departId);

    Employee findByUserId(Integer userId);

    Employee findEmployee(Integer id);

    Employee saveEmployee(Employee employee);

    void deleteEmployee(Integer id);
}
