package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.entity.Project;

import java.util.List;

@Repository("projectDao")
@Transactional
public class ProjectDaoImpl extends AbstractDao<Integer, Project> implements ProjectDao {
    @Override
    public Project findProject(Integer id) {
        return getByKey(id);
    }

    @Override
    public List<Project> findAllProjects() {
        Criteria criteria = createEntityCriteria();
        return (List<Project>) criteria.list();
    }

    @Override
    public Project saveProject(Project project) {
        persist(project);
        flush();
        return project;
    }

    @Override
    public void deleteProject(Integer id) {
        Query query = getSession().createSQLQuery("delete from PROJECTS where id = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }
}
