package rtu.master.work.dao;

import rtu.master.work.entity.ClientContactInfo;

import java.util.List;

public interface ClientContactInfoDao {

    ClientContactInfo findClientContactInfoById(Integer id);

    List<ClientContactInfo> findClientContactInfo(Integer clientId);

    void saveClientContactInfo(ClientContactInfo clientContactInfo);

    void deleteClientContactInfoById(Integer id);

    void deleteClientContactInfo(Integer clientId);

}
