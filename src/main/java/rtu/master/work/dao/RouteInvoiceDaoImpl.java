package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.entity.RouteInvoice;

import java.util.List;

@Repository("routeInvoiceDao")
@Transactional
public class RouteInvoiceDaoImpl extends AbstractDao<Integer, RouteInvoice> implements RouteInvoiceDao{

    @Override
    public List<RouteInvoice> findRouteInvoices(Integer routeId) {
        Criteria criteria = createEntityCriteria();
        return (List<RouteInvoice>) criteria
                .add(Restrictions.eq("routeId", routeId))
                .list();
    }
}
