package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rtu.master.work.entity.Client;

import javax.transaction.Transactional;
import java.util.List;

@Repository("clientDao")
@Transactional
public class ClientDaoImpl extends AbstractDao<Integer, Client> implements ClientDao {

    @Override
    public List<Client> findAllClients() {
        Criteria criteria = createEntityCriteria();
        return (List<Client>) criteria.list();
    }

    @Override
    public Client findClient(Integer id) {
        return getByKey(id);
    }

    @Override
    public Client findByPosNr(Integer posNr) {
        Criteria criteria = createEntityCriteria();
        return (Client) criteria
                .add(Restrictions.eq("posNr", posNr))
                .uniqueResult();
    }

    @Override
    public Client saveClient(Client client) {
        persist(client);
        flush();
        return client;
    }

    @Override
    public void deleteClient(Integer id) {
        Query query = getSession().createSQLQuery("delete from CLIENTS where ID = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    @Override
    public Client findClientByPosNr(Integer posNr) {
        Criteria criteria = createEntityCriteria();
        return (Client) criteria
                .add(Restrictions.eq("posNr", posNr))
                .uniqueResult();
    }

    @Override
    public Client findClientByRegNr(String regNr) {
        Criteria criteria = createEntityCriteria();
        return (Client) criteria
                .add(Restrictions.eq("regNr", regNr))
                .uniqueResult();
    }
}
