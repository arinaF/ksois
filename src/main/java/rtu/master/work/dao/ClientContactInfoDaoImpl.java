package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rtu.master.work.entity.ClientContactInfo;

import javax.transaction.Transactional;
import java.util.List;

@Repository("clientContactInfoDao")
@Transactional
public class ClientContactInfoDaoImpl extends AbstractDao<Integer, ClientContactInfo> implements ClientContactInfoDao {

    @Override
    public ClientContactInfo findClientContactInfoById(Integer id) {
        Criteria criteria = createEntityCriteria();
        return (ClientContactInfo) criteria
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    @Override
    public List<ClientContactInfo> findClientContactInfo(Integer clientId) {
        Criteria criteria = createEntityCriteria();
        return (List<ClientContactInfo>) criteria
                .add(Restrictions.eq("clientId", clientId))
                .list();
    }

    @Override
    public void saveClientContactInfo(ClientContactInfo clientContactInfo) {
        persist(clientContactInfo);
    }

    @Override
    public void deleteClientContactInfoById(Integer id) {
        Query query = getSession().createSQLQuery("delete from CLIENTS_CONTACT_INFO where ID = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    @Override
    public void deleteClientContactInfo(Integer clientId) {
        Query query = getSession().createSQLQuery("delete from CLIENTS_CONTACT_INFO where CLIENT_ID = :clientId");
        query.setInteger("clientId", clientId);
        query.executeUpdate();
    }
}
