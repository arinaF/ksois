package rtu.master.work.dao;

import rtu.master.work.entity.Route;

import java.util.List;

public interface RouteDao {

    Route findRoute(Integer id);

    List<Route> findAllRoutes();

    List<Route> findAllCurrentDayRoutes();

    List<Route> findDriverRoutes(Integer driverId);

}
