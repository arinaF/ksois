package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rtu.master.work.entity.Classifier;

import javax.swing.*;
import javax.transaction.Transactional;
import java.util.List;

@Repository("classifierDao")
@Transactional
public class ClassifierDaoImpl extends AbstractDao<Integer, Classifier> implements ClassifierDao {

    @Override
    @SuppressWarnings("unchecked")
    public List<Classifier> findAllClassifiers(String parentCode) {
        Criteria criteria = createEntityCriteria();
        return (List<Classifier>) criteria
                .add(Restrictions.eq("parentCode", parentCode))
                .list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Classifier> findAllClassifiers(String parentCode, String type) {
        Criteria criteria = createEntityCriteria();
        return (List<Classifier>) criteria
                .add(Restrictions.eq("parentCode", parentCode))
                .add(Restrictions.eq("type", type))
                .list();
    }

    @Override
    public Classifier findClassifier(String code) {
        Criteria criteria = createEntityCriteria();
        return (Classifier) criteria
                .add(Restrictions.eq("code", code))
                .uniqueResult();
    }
}
