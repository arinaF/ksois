package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.entity.ReplannedInvoice;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

@Repository("replannedInvoiceDao")
@Transactional
public class ReplannedInvoiceDaoImpl extends AbstractDao<Integer, ReplannedInvoice> implements ReplannedInvoiceDao {

    @Override
    public ReplannedInvoice findReplannedInvoice(Integer id) {
        return getByKey(id);
    }

    @Override
    public List<ReplannedInvoice> findAllReplannedInvoices() {
        Criteria criteria = createEntityCriteria();
        return (List<ReplannedInvoice>) criteria.list();
    }

    @Override
    public void saveReplannedInvoice(ReplannedInvoice replannedInvoice) {
        persist(replannedInvoice);
        flush();
    }

    @Override
    public void deleteReplannedInvoice(Integer id) {
        Query query = getSession().createSQLQuery("delete from REPLANNED_INVOICES where id = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    @Override
    public int countReplannedInvoices(Integer invoiceId, Date deliveryDate, Integer routeId) {
        Query query = getSession().createSQLQuery("select count(*) from REPLANNED_INVOICES where INVOICE_ID = :invoiceId" +
                " and date_format(DELIVERY_DATE, '%Y-%m-%d') = date_format(:deliveryDate, '%Y-%m-%d')" +
                " and ROUTE_ID = :routeId");
        query.setInteger("invoiceId", invoiceId);
        query.setInteger("routeId", routeId);
        query.setDate("deliveryDate", deliveryDate);
        Integer count = ((BigInteger) query.uniqueResult()).intValue();
        return count;
    }
}
