package rtu.master.work.dao;

import rtu.master.work.entity.Project;

import java.util.List;

public interface ProjectDao {

    Project findProject(Integer id);

    List<Project> findAllProjects();

    Project saveProject(Project project);

    void deleteProject(Integer id);
}
