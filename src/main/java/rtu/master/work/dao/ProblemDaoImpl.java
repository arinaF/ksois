package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import rtu.master.work.entity.Problem;

import java.math.BigInteger;
import java.util.List;

@Repository("problemDao")
@Transactional
public class ProblemDaoImpl extends AbstractDao<Integer, Problem> implements ProblemDao {
    @Override
    public List<Problem> findAllProblems() {
        Criteria criteria = createEntityCriteria();
        return (List<Problem>) criteria
                .addOrder(Order.desc("regDate"))
                .list();
    }

    @Override
    public List<Problem> findAllOperatorProblems(Integer operatorId) {
        Criteria criteria = createEntityCriteria();
        criteria.add(Restrictions.eq("operatorId", operatorId))
                .add(Restrictions
                        .sqlRestriction("date_format(REG_DATE, '%Y-%m-%d') = date_format(sysdate(), '%Y-%m-%d')"))
                .addOrder(Order.desc("regDate"));
        return (List<Problem>) criteria.list();
    }

    @Override
    public List<Problem> findAllDriverProblems(Integer driverId) {
        Criteria criteria = createEntityCriteria();
        return (List<Problem>) criteria
                .add(Restrictions.eq("driverId", driverId))
                .addOrder(Order.desc("regDate"))
                .list();
    }

    @Override
    public List<Problem> findAllClientProblems(Integer clientId) {
        Criteria criteria = createEntityCriteria();
        return (List<Problem>) criteria
                .add(Restrictions.eq("clientId", clientId))
                .addOrder(Order.desc("regDate"))
                .list();
    }

    @Override
    public Problem findProblem(Integer id) {
        return getByKey(id);
    }

    @Override
    public Problem saveProblem(Problem problem) {
        persist(problem);
        flush();
        return problem;
    }

    @Override
    public void deleteProblem(Integer id) {
        Query query = getSession().createSQLQuery("delete from PROBLEMS where ID = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }

    @Override
    public Integer currentDaysProblemCount(Integer operatorId) {
        Query query = getSession()
                .createSQLQuery("select count(*) from PROBLEMS where OPERATOR_ID = :operatorId " +
                        "and date_format(REG_DATE, '%Y-%m-%d') = date_format(sysdate(), '%Y-%m-%d')");
        query.setInteger("operatorId", operatorId);
        Integer count = ((BigInteger) query.uniqueResult()).intValue();

        return count;
    }
}
