package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rtu.master.work.entity.Department;

import javax.transaction.Transactional;
import java.util.List;

@Repository("departmentDao")
@Transactional
public class DepartmentDaoImpl extends AbstractDao<Integer, Department> implements DepartmentDao {

    @Override
    public Department findDepartment(Integer id) {
        Criteria criteria = createEntityCriteria();
        return (Department) criteria
                .add(Restrictions.eq("id", id))
                .uniqueResult();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Department> findDepartments() {
        Criteria criteria = createEntityCriteria();
        return (List<Department>) criteria.list();
    }
}
