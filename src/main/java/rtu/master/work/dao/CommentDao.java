package rtu.master.work.dao;

import rtu.master.work.entity.Comment;

import java.util.List;

public interface CommentDao {

    List<Comment> findAllComments(Integer problemId);

    Comment findComment(Integer id);

    Comment saveComment(Comment comment);

    void deleteComment(Integer id);
}
