package rtu.master.work.dao;

import rtu.master.work.entity.EmployeeContactInfo;

import java.util.List;

public interface ContactInfoDao {

    EmployeeContactInfo findEmployeeContactInfoById(Integer id);

    List<EmployeeContactInfo> findEmployeeContactInfo(Integer employeeId);

    void saveEmployeeContactInfo(EmployeeContactInfo employeeContactInfo);

    void deleteEmployeeContactInfoById(Integer id);

    void deleteEmployeeContactInfo(Integer employeeId);

}
