package rtu.master.work.dao;

import rtu.master.work.entity.Classifier;

import java.util.List;

public interface ClassifierDao {

    List<Classifier> findAllClassifiers(String parentCode);

    List<Classifier> findAllClassifiers(String parentCode, String type);

    Classifier findClassifier(String code);
}
