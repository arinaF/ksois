package rtu.master.work.dao;

import rtu.master.work.entity.Invoice;

import java.util.List;

public interface InvoiceDao {

    Invoice findInvoice(Integer id);

    Invoice findInvoice(String identifier);

    List<Invoice> findAllInvoices();

    Invoice saveInvoice(Invoice invoice);

    void deleteInvoice(Integer id);
}

