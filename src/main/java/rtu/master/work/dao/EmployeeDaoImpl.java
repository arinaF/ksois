package rtu.master.work.dao;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rtu.master.work.entity.Employee;

import javax.transaction.Transactional;
import java.util.List;

@Repository("employeeDao")
@Transactional
public class EmployeeDaoImpl extends AbstractDao<Integer, Employee> implements EmployeeDao {

    @SuppressWarnings("unchecked")
    public List<Employee> findAllEmployees() {
        Criteria criteria = createEntityCriteria();
        return (List<Employee>) criteria.list();
    }

    @SuppressWarnings("unchecked")
    public List<Employee> findAllEmployees(Integer departId) {
        Criteria criteria = createEntityCriteria();
        return (List<Employee>) criteria
                .add(Restrictions.eq("departId", departId))
                .list();
    }

    @Override
    public Employee findByUserId(Integer userId) {
        Criteria criteria = createEntityCriteria();
        return (Employee) criteria
                .add(Restrictions.eq("userId", userId))
                .uniqueResult();
    }

    public Employee findEmployee(Integer id) {
        return getByKey(id);
    }

    @Override
    public Employee saveEmployee(Employee employee) {
        persist(employee);
        flush();
        return employee;
    }

    public void deleteEmployee(Integer id) {
        Query query = getSession().createSQLQuery("delete from EMPLOYEES where ID = :id");
        query.setInteger("id", id);
        query.executeUpdate();
    }
}
