package rtu.master.work.dao;

import rtu.master.work.dto.RouteInvoiceDto;
import rtu.master.work.entity.RouteInvoice;

import java.util.List;

public interface RouteInvoiceDao {

    List<RouteInvoice> findRouteInvoices(Integer routeId);

}
