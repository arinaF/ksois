package rtu.master.work.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import rtu.master.work.dto.EmployeeDto;
import rtu.master.work.exception.ValidationException;
import rtu.master.work.service.ProblemService;

import java.util.Arrays;

@Component
public class EmployeeDtoValidator implements Validator {

    private final String MIN_LENGTH = "garums jābūt lielāks par 3 simboliem";
    private final String MAX_LENGTH = "garums nevar būt lielāks par 100 simboliem";

    @Autowired
    ProblemService problemService;

    @Override
    public boolean supports(Class<?> aClass) {
        return EmployeeDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        EmployeeDto dto = (EmployeeDto) o;
        if (dto != null) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "Vārds nevar būt tukšs");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "Uzvārds nevar būt tukšs");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "department.id", "Nodaļa nevar būt tukša");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "position.code", "Amats nevar būt tukšs");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "region.code", "Reģions nevar būt tukšs");

            if (StringUtils.isNotBlank(dto.getName()) && StringUtils.length(dto.getName()) < 3) {
                errors.reject("Vārda ".concat(MIN_LENGTH));
            }
            if (StringUtils.isNotBlank(dto.getName()) && StringUtils.length(dto.getName()) > 100) {
                errors.reject("Vārda ".concat(MAX_LENGTH));
            }
            if (StringUtils.isNotBlank(dto.getSurname()) && StringUtils.length(dto.getSurname()) < 3) {
                errors.reject("Uzvārda ".concat(MIN_LENGTH));
            }
            if (StringUtils.isNotBlank(dto.getSurname()) && StringUtils.length(dto.getSurname()) > 100) {
                errors.reject("Uzvārda ".concat(MAX_LENGTH));
            }

            dto.getEmployeeContactInfoList().forEach(contact -> {
                if (StringUtils.isBlank(contact.getType()) || StringUtils.isBlank(contact.getContact())) {
                    errors.reject("Kontaktinformācijas lauki nevar būt tukši");
                }
            });
        }
    }

    public void validateOnDelete(EmployeeDto dto) {
        if (!CollectionUtils.isEmpty(problemService.findAllDriverProblems(dto.getId()))) {
            throw new ValidationException(Arrays.asList("Darbinieku nav iespejams dzēst. Sistēmā ir piereģistrētas saistītas problēmas."));
        }
    }
}
