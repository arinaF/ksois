package rtu.master.work.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import rtu.master.work.entity.Project;

@Component
public class ProjectValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return Project.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "code", "Projekta kods nevar būt tukšs");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "Projekta apraksts nevar būt tukšs");
    }
}
