package rtu.master.work.validator;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;
import rtu.master.work.dto.ProblemDto;
import rtu.master.work.entity.Client;
import rtu.master.work.entity.Invoice;
import rtu.master.work.service.ClientService;
import rtu.master.work.service.InvoiceService;

@Component
public class ProblemDtoValidator implements Validator {

    @Autowired
    InvoiceService invoiceService;

    @Autowired
    ClientService clientService;

    @Override
    public boolean supports(Class<?> aClass) {
        return ProblemDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ProblemDto dto = (ProblemDto) o;
        if (dto != null) {
            if (dto.getClientPosNr() == null) {
                errors.reject("POS numurs nevar būt tukšs");
            }

            if (StringUtils.isBlank(dto.getDescription())) {
                errors.reject("Apraksts nevar būt tukšs");
            }

            if (dto.getProjectId() == null) {
                errors.reject("Projekts nevar būt tukšs");
            }

            if (dto.getWarehouseId() == null) {
                errors.reject("Noliktava nevar būt tukša");
            }

            if (StringUtils.isBlank(dto.getAction())) {
                errors.reject("Rīcība nevar būt tukša");
            }

            if (StringUtils.isBlank(dto.getReason())) {
                errors.reject("Iemesls nevar būt tukšs");
            }

            if (StringUtils.isBlank(dto.getSource())) {
                errors.reject("Lauks 'No ka pieņemts' nevar būt tukšs");
            }

            if (StringUtils.isBlank(dto.getRespDepartment())) {
                errors.reject("Atbildīgs nevar būt tukšs");
            }

            if (StringUtils.isNotBlank(dto.getInvoiceIdentifier())) {
                Invoice invoice = invoiceService.findInvoice(dto.getInvoiceIdentifier());
                if (invoice != null) {
                    Client client = clientService.findClient(invoice.getClientId());
                    if (ObjectUtils.notEqual(client.getPosNr(), dto.getClientPosNr())) {
                        errors.reject("Pavadzīmē norādīts klients nesakrīt ar ievadīto POS numuru");
                    }

                    if (invoice.getProjectId() != dto.getProjectId()) {
                        errors.reject("Pavadzīmē norādīts projekts nesakrīt ar ievadīto projektu");
                    }

                    if (invoice.getWarehouseId() != dto.getWarehouseId()) {
                        errors.reject("Pavadzīmē norādīta noliktava nesakrīt ar ievadīto noliktavu");
                    }
                }
            }

            if (dto.getClientPosNr() != null) {
                Client client = clientService.findByPosNr(dto.getClientPosNr());
                if (client == null) {
                    errors.reject("Klients ar POS numuru " + dto.getClientPosNr() + " nav atrasts sistēmā");
                }
            }
        }
    }
}