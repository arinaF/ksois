package rtu.master.work.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import rtu.master.work.dto.UserDto;

@Component
public class UserDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return UserDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDto dto = (UserDto) o;
        if (dto != null) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "Lietotājvārds nevar būt tukšs");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "Parole nevar būt tukša");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "role", "Loma nevar būt tukša");

            if (StringUtils.isNotBlank(dto.getPassword()) && StringUtils.isBlank(dto.getConfirmedPassword())) {
                errors.reject("Atkārtojiet paroli");
            }
            if (StringUtils.isNotBlank(dto.getUsername()) && !StringUtils.contains(dto.getUsername(), ".")) {
                errors.reject("Lietotājvārds ir jāsatur vārdu un uzvārdu, kas ir atdalīti ar punktu: 'name.surname'");
            }
            if (dto.getEmployeeId() == null) {
                errors.reject("Darbinieks nav izvēlēts");
            }
            if (StringUtils.isNotBlank(dto.getPassword()) &&
                    StringUtils.isNotBlank(dto.getConfirmedPassword()) &&
                    !StringUtils.equals(dto.getPassword(), dto.getConfirmedPassword())) {
                errors.reject("Parole un atkārtoti ievadīta parole nesakrīt");
            }
        }
    }
}
