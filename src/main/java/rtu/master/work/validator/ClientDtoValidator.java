package rtu.master.work.validator;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;
import rtu.master.work.dao.ClientDao;
import rtu.master.work.dto.ClientDto;
import rtu.master.work.entity.Client;
import rtu.master.work.exception.ValidationException;
import rtu.master.work.service.ProblemService;

import java.util.Arrays;

@Component
public class ClientDtoValidator implements Validator {

    @Autowired
    private ClientDao dao;

    @Autowired
    private ProblemService problemService;

    @Override
    public boolean supports(Class<?> aClass) {
        return ClientDto.class.isAssignableFrom(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ClientDto dto = (ClientDto) o;
        if (dto != null) {
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "posNr", "POS numurs nevar būt tukšs");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "regNr", "Reģistrācijas numurs nevar būt tukšs");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "Nosaukums nevar būt tukšs");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "Adrese nevar būt tukša");
            ValidationUtils.rejectIfEmptyOrWhitespace(errors, "region", "Reģions nevar būt tukšs");

            dto.getClientContactInfoList().forEach(contact -> {
                if (StringUtils.isBlank(contact.getType()) || StringUtils.isBlank(contact.getContact())) {
                    errors.reject("Kontaktinformācijas lauki nevar būt tukši");
                }
            });

            if (dto.getPosNr() != null && StringUtils.isNotBlank(dto.getPosNr().toString())) {
                if (dto.getPosNr().toString().length() != 7) {
                    errors.reject("POS numurs ir jābūt skaitlis, kas sastāv no 7 cipariem");
                }
                if (!dto.getPosNr().toString().startsWith("19")) {
                    errors.reject("POS numuram būtu jāsākas ar 19");
                }
                for (char c : dto.getPosNr().toString().toCharArray()) {
                    if (!Character.isDigit(c)) {
                        errors.reject("POS numurs ir jābūt skaitlis, kas sākas ar 19");
                    }
                }
                Client clientFromDB = dao.findClientByPosNr(dto.getPosNr());
                if (clientFromDB != null && !clientFromDB.getId().equals(dto.getId())) {
                    errors.reject("POS numurs " + dto.getPosNr() + " pieder citam klientam " + clientFromDB.getName());
                }
            }

            if (StringUtils.isNotBlank(dto.getRegNr())) {
                if (dto.getRegNr().length() != 11) {
                    errors.reject("Reģistrācijas numuram ir jābūt 11 cipari");
                }
                if (!dto.getRegNr().startsWith("4") && !dto.getRegNr().startsWith("5")) {
                    errors.reject("Reģistrācijas numuram būtu jāsākas ar '4' vai '5'");
                }
                Client clientFromDB = dao.findClientByRegNr(dto.getRegNr());
                if (clientFromDB != null && !clientFromDB.getRegNr().equals(dto.getRegNr())) {
                    errors.reject("Reģistrācijas numurs " + dto.getRegNr() + " pieder citam klientam " + clientFromDB.getName());
                }
            }
        }
    }

    public void validateOnDelete(ClientDto dto) {
        if (!CollectionUtils.isEmpty(problemService.findAllClientProblems(dto.getId()))) {
            throw new ValidationException(Arrays.asList("Klientu nav iespejams dzēst. Sistēmā ir piereģistrētas saistītas problēmas."));
        }
    }
}
