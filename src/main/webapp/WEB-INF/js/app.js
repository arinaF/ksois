var app = angular.module('ksoisApp', ['ngMap', 'ui.router', 'ui-notification', 'ui.bootstrap']);

app.config(['$stateProvider', '$urlRouterProvider',
    function ($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('home', {
                url: '/',
                controller: 'ProblemsController',
                templateUrl: 'views/problems.html'
            })
            .state('myProblems', {
                url: '/myProblems',
                controller: 'OperatorProblemsController',
                templateUrl: 'views/operatorProblems.html'
            })
            .state('problems', {
                url: '/problems',
                controller: 'ProblemsController',
                templateUrl: 'views/problems.html'
            })
            .state('problem', {
                url: '/problems/problem/:id',
                controller: 'ProblemProfileController',
                templateUrl: 'views/problemProfile.html'
            })
            .state('newProblem', {
                url: '/problems/newProblem',
                controller: 'NewProblemController',
                templateUrl: 'views/newProblem.html'
            })
            .state('newEmployee', {
                url: '/newEmployee/:type',
                controller: 'NewEmployeeController',
                templateUrl: 'views/newEmployee.html'
            })
            .state('clients', {
                url: '/clients',
                controller: 'ClientsController',
                templateUrl: 'views/clients.html'
            })
            .state('client', {
                url: '/clients/client/:id',
                controller: 'ClientProfileController',
                templateUrl: 'views/clientProfile.html'
            })
            .state('newClient', {
                url: '/newClient',
                controller: 'NewClientController',
                templateUrl: 'views/newClient.html'
            })
            .state('managers', {
                url: '/managers',
                controller: 'ManagersController',
                templateUrl: 'views/managers.html'
            })
            .state('manager', {
                url: '/managers/manager/:id',
                controller: 'ManagerProfileController',
                templateUrl: 'views/managerProfile.html'
            })
            .state('projects', {
                url: '/projects',
                controller: 'ProjectsController',
                templateUrl: 'views/projects.html'
            })
            .state('routes', {
                url: '/routes',
                controller: 'RoutesController',
                templateUrl: 'views/routes.html'
            })
            .state('route', {
                url: '/routes/route/:id',
                controller: 'RouteProfileController',
                templateUrl: 'views/routeProfile.html'
            })
            .state('drivers', {
                url: '/drivers',
                controller: 'DriversController',
                templateUrl: 'views/drivers.html'
            })
            .state('driver', {
                url: '/drivers/driver/:id',
                controller: 'DriverProfileController',
                templateUrl: 'views/driverProfile.html'
            })
            .state('rescheduled', {
                url: '/rescheduled',
                controller: 'ReplannedInvoicesController',
                templateUrl: 'views/replannedInvoices.html'
            })
            .state('warehouseEmployee', {
                url: '/warehouses/employee/:id',
                controller: 'WarehouseEmplProfileController',
                templateUrl: 'views/warehouseEmplProfile.html'
            })
            .state('warehouses', {
                url: '/warehouses',
                controller: 'WarehouseController',
                templateUrl: 'views/warehouses.html'
            })
            .state('users', {
                url: '/users',
                controller: 'UsersController',
                templateUrl: 'views/users.html'
            })
            .state('newUser', {
                url: '/newUser',
                controller: 'NewUserController',
                templateUrl: 'views/newUser.html'
            })
            .state('login', {
                url: '/login',
                templateUrl: 'login.jsp'
            });

        $urlRouterProvider.otherwise('/');

    }

]);
