app.factory('RouteService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/ksois/routes/';

    return {
        fetchAllRoutes: function fetchAllRoutes() {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Routes');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        fetchAllCurrentDayRoutes: function fetchAllCurrentDayRoutes() {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + "currentDay")
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Routes');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        fetchRoute: function fetchRoute(id) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Route');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        fetchRouteInvoices: function fetchRouteInvoices(id) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + id + "/invoices")
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Route Invoices');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        updateRoute: function updateRoute(route) {
            var deferred = $q.defer();
            $http.put(REST_SERVICE_URI, route)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        deferred.reject(errResponse);
                    }
                );

            return deferred.promise;
        }
    }
}]);
