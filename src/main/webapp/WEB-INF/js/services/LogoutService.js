app.factory('LogoutService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/ksois/logout';

    return {
        logout: function logout() {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while logging out');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }
    }
}]);