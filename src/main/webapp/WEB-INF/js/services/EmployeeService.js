app.factory('EmployeeService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/ksois/employees/';

    return {
        fetchAllEmployees: function fetchAllEmployees(departType, departCode, id) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI, {
                params: {departType: departType, departCode: departCode, departId: id}
            })
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Employees');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        fetchEmployee: function fetchEmployee(id) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Employee');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        updateEmployee: function updateEmployee(id, employee) {
            var deferred = $q.defer();
            $http.put(REST_SERVICE_URI + id, employee)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        deleteEmployee: function deleteEmployee(id) {
            var deferred = $q.defer();
            $http.delete(REST_SERVICE_URI + id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while deleting Employee');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        saveEmployee: function saveEmployee(employee) {
            var deferred = $q.defer();
            $http.post(REST_SERVICE_URI, employee)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }
    }
}]);
