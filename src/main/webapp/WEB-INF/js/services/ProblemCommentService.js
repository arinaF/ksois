app.factory('ProblemCommentService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/ksois/comments/';

    return {
        fetchAllComments: function fetchAllComments(problemId) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + problemId)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Comments');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        deleteComment: function deleteComment(id) {
            var deferred = $q.defer();
            $http.delete(REST_SERVICE_URI + id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while deleting Comment');
                        deferred.reject(errResponse);
                    }
                );
        },

        saveOrUpdateComment: function saveOrUpdateComment(comment) {
            var deferred = $q.defer();
            $http.post(REST_SERVICE_URI, comment)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }
    }
}]);
