app.factory('UserService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/ksois/';

    return {
        getUser: function getUser() {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + 'username')
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching username');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },
        fetchPermissions: function fetchPermissions() {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + 'permissions')
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },
        fetchEditPermissions: function fetchEditPermissions(operatorId) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + 'permissions/edit/' + operatorId)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },
        fetchAllUsers: function fetchAllUsers() {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + 'users/')
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },
        updateUser: function updateUser(user) {
            var deferred = $q.defer();
            $http.put(REST_SERVICE_URI + 'users/', user)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        saveUser: function saveUser(user) {
            var deferred = $q.defer();
            $http.post(REST_SERVICE_URI + 'users/', user)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        deleteUser: function deleteUser(id) {
            var deferred = $q.defer();
            $http.delete(REST_SERVICE_URI + 'users/' + id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while deleting User');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }
    }
}]);