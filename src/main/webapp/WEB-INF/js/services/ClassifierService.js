app.factory('ClassifierService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/ksois/classifiers/';

    return {
        fetchAllClassifiers: function fetchAllClassifiers(parentCode, type) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI, {
                params: {parentCode: parentCode, type: type}
            })
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Classifier');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }
    }
}]);