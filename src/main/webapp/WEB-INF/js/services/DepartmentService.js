app.factory('DepartmentService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/ksois/departments/';

    return {
        fetchAllDepartmentsByTypeOrCode: function fetchAllDepartmentsByTypeOrCode(type, code) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + 'codeAndType', {
                params: {type: type, code: code}
            })
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Departments');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        fetchAllDepartments: function fetchAllDepartments() {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Departments');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }
    }
}]);