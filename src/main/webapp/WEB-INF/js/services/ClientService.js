app.factory('ClientService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/ksois/clients/';

    return {
        fetchAllClients: function fetchAllClients() {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Clients');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        fetchClient: function fetchClient(id) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Client');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        updateClient: function updateClient(id, client) {
            var deferred = $q.defer();
            $http.put(REST_SERVICE_URI + id, client)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        deleteClient: function deleteClient(id) {
            var deferred = $q.defer();
            $http.delete(REST_SERVICE_URI + id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while deleting Client');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        saveClient: function saveClient(client) {
            var deferred = $q.defer();
            $http.post(REST_SERVICE_URI, client)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        }
    }
}]);
