app.factory('ProblemService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/ksois/problems/';

    return {
        fetchAllProblems: function fetchAllProblems(operatorOnly) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI, {
                params: {operatorOnly: operatorOnly}
            })
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Problems');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        fetchProblem: function fetchProblem(id) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Problem');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        fetchDriverProblems: function fetchDriverProblems(driverId) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + 'driver/' + driverId)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Driver Problem');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        fetchClientProblems: function fetchClientProblems(clientId) {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI + 'client/list/' + clientId)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Client Problem');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        updateProblem: function updateProblem(id, problem) {
            var deferred = $q.defer();
            $http.put(REST_SERVICE_URI + id, problem)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        deleteProblem: function deleteProblem(id) {
            var deferred = $q.defer();
            $http.delete(REST_SERVICE_URI + id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while deleting Problem');
                        deferred.reject(errResponse);
                    }
                );
        },

        saveProblems: function saveProblems(problems) {
            var deferred = $q.defer();
            $http.post(REST_SERVICE_URI, problems)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        fetchPprData: function fetchPprData(invoice) {
            var deffered = $q.defer();
            $http.get(REST_SERVICE_URI + "/ppr/" + invoice)
                .then(
                    function (response) {
                        deffered.resolve(response.data);
                    },
                    function (errResponse) {
                        deffered.reject(errResponse);
                    }
                );
            return deffered.promise;
        },

        fetchClientData: function fetchClientData(posNr) {
            var deffered = $q.defer();
            $http.get(REST_SERVICE_URI + "/client/" + posNr)
                .then(
                    function (response) {
                        deffered.resolve(response.data);
                    },
                    function (errResponse) {
                        deffered.reject(errResponse);
                    }
                );
            return deffered.promise;
        }
    }
}]);
