app.factory('ProjectService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/ksois/projects/';

    return {
        fetchAllProjects: function fetchAllProjects() {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Projects');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        saveOrUpdateProjects: function saveOrUpdateProjects(projects) {
            var deferred = $q.defer();
            projects.forEach(function (project) {
                $http.post(REST_SERVICE_URI, project)
                    .then(
                        function (response) {
                            deferred.resolve(response.data);
                        },
                        function (errResponse) {
                            deferred.reject(errResponse);
                        }
                    );
            });
            return deferred.promise;
        },

        deleteProject: function deleteProject(id) {
            var deferred = $q.defer();
            $http.delete(REST_SERVICE_URI + id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while deleting Project');
                        deferred.reject(errResponse);
                    }
                );
        }
    }
}]);
