app.factory('ReplannedInvoiceService', ['$http', '$q', function ($http, $q) {

    var REST_SERVICE_URI = 'http://localhost:8080/ksois/replannedInvoices/';

    return {
        fetchAllReplannedInvoices: function fetchAllReplannedInvoices() {
            var deferred = $q.defer();
            $http.get(REST_SERVICE_URI)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        console.error('Error while fetching Replanned Invoices');
                        deferred.reject(errResponse);
                    }
                );
            return deferred.promise;
        },

        updateReplannedInvoice: function updateReplannedInvoice(id, invoice) {
            var deferred = $q.defer();
            $http.put(REST_SERVICE_URI + id, invoice)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        deferred.reject(errResponse);
                    }
                );

            return deferred.promise;
        },

        saveReplannedInvoices: function saveReplannedInvoices(invoices) {
            var deferred = $q.defer();
            $http.post(REST_SERVICE_URI, invoices)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        deferred.reject(errResponse);
                    }
                );

            return deferred.promise;
        },

        deleteReplannedInvoice: function deleteReplannedInvoice(id) {
            var deferred = $q.defer();
            $http.delete(REST_SERVICE_URI + id)
                .then(
                    function (response) {
                        deferred.resolve(response.data);
                    },
                    function (errResponse) {
                        deferred.reject(errResponse);
                    }
                );

            return deferred.promise;
        }
    }
}]);
