app.controller('NewClientController', ['$scope', 'ClassifierService', 'ClientService', '$stateParams', '$window', 'Notification',
    function ($scope, ClassifierService, ClientService, $stateParams, $window, Notification) {

        $scope.newClient = {
            id: null,
            posNr: null,
            regNr: '',
            name: '',
            address: '',
            lat: null,
            lng: null,
            region: {
                id: null,
                parentCode: 'REGION',
                code: '',
                name: '',
                type: ''
            },
            active: true,
            clientContactInfoList: []
        };


        $scope.regions = [];
        $scope.contactInfoTypes = [];

        fetchAutocomplete();

        function fetchAutocomplete() {
            ClassifierService.fetchAllClassifiers('REGION', null)
                .then(
                    function (d) {
                        $scope.regions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Regions');
                    }
                );
            ClassifierService.fetchAllClassifiers('CONTACT_INFO_TYPE', null)
                .then(
                    function (d) {
                        $scope.contactInfoTypes = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching ContactInfoTypes');
                    }
                );
        }

        $scope.addNewContactInfo = function addNewContactInfo() {
            $scope.newContact = {id: null, clientId: null, type: '', contact: '', contactPerson: '', active: true};
            $scope.newClient.clientContactInfoList.push($scope.newContact);
        };

        $scope.deleteContactInfo = function deleteContactInfo(index) {
            $scope.newClient.clientContactInfoList.splice(index, 1);
        };

        $scope.saveNewClient = function saveNewClient() {
            ClientService.saveClient($scope.newClient)
                .then(
                    function (d) {
                        $scope.createdId = d;
                        $window.history.back();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                )
        };
    }]);