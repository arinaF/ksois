app.controller('ProblemsController', ['$scope', 'ProblemService', '$state',
    function ($scope, ProblemService, $state) {

        $scope.problems = [];

        fetchAllProblems();

        function fetchAllProblems() {
            ProblemService.fetchAllProblems(false)
                .then(
                    function (d) {
                        $scope.problems = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Problems');
                    }
                );
        }

        $scope.openProblemDetails = function (id) {
            $state.go('problem', {id: id});
        };

        $scope.registerNewProblem = function () {
            $state.go('newProblem');
        };
    }]);