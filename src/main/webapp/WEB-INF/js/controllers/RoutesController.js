app.controller('RoutesController', ['$scope', 'RouteService', 'EmployeeService', 'UserService', '$state', 'Notification',
    function ($scope, RouteService, EmployeeService, UserService, $state, Notification) {

        $scope.routes = [];

        $scope.currentDayRoutes = [];

        $scope.permissions = [];

        $scope.drivers = [];

        fetchAllRoutes();
        fetchPermissions();

        function fetchAllRoutes() {
            RouteService.fetchAllRoutes()
                .then(
                    function (d) {
                        $scope.routes = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Routes');
                    }
                );
            RouteService.fetchAllCurrentDayRoutes()
                .then(
                    function (d) {
                        $scope.currentDayRoutes = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Current Day Routes');
                    }
                )
        };

        function fetchPermissions() {
            UserService.fetchPermissions()
                .then(
                    function (d) {
                        $scope.permissions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                    }
                )
        };

        $scope.hasPermission = function hasPermission(role) {
            return $.inArray(role, $scope.permissions) > -1;
        };

        $scope.openRouteDetails = function (id) {
            $state.go('route', {id: id});
        };

    }]);