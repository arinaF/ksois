app.controller('WarehouseEmplProfileController', ['$scope', 'EmployeeService', 'ClassifierService', 'DepartmentService', 'UserService', '$state', '$stateParams', 'Notification',
    function ($scope, EmployeeService, ClassifierService, DepartmentService, UserService, $state, $stateParams, Notification) {

        $scope.readonly = true;
        $scope.departments = [];
        $scope.regions = [];
        $scope.positions = [];
        $scope.contactInfoTypes = [];

        $scope.warhempl = {};
        $scope.warhemplContactInfo = [];
        $scope.permissions = [];

        fetchWarhempl();
        fetchAutocomplete();
        fetchPermissions();

        $scope.addNewContactInfo = function addNewContactInfo() {
            $scope.newContact = {id: null, employeeId: $stateParams.id, type: '', contact: '', active: true};
            $scope.warhempl.employeeContactInfoList.push($scope.newContact);
        };

        $scope.deleteContactInfo = function deleteContactInfo(index) {
            $scope.warhempl.employeeContactInfoList.splice(index, 1);
        };


        $scope.changeReadonly = function changeReadonly() {
            $scope.readonly = !$scope.readonly;
        };

        $scope.hasPermission = function hasPermission(role) {
            return $.inArray(role, $scope.permissions) > -1;
        };

        $scope.cancelEdit = function cancelEdit() {
            fetchWarhempl();
            $scope.changeReadonly();
        };

        function fetchAutocomplete() {
            DepartmentService.fetchAllDepartmentsByTypeOrCode('WAREHOUSE', null)
                .then(
                    function (d) {
                        $scope.departments = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Departments')
                    }
                );
            ClassifierService.fetchAllClassifiers('REGION', null)
                .then(
                    function (d) {
                        $scope.regions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Regions');
                    }
                );
            ClassifierService.fetchAllClassifiers('POSITION', 'WAREHOUSE')
                .then(
                    function (d) {
                        $scope.positions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Positions');
                    }
                );
            ClassifierService.fetchAllClassifiers('CONTACT_INFO_TYPE', null)
                .then(
                    function (d) {
                        $scope.contactInfoTypes = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching ContactInfoTypes');
                    }
                );
        }

        function fetchWarhempl() {
            EmployeeService.fetchEmployee($stateParams.id)
                .then(
                    function (d) {
                        $scope.warhempl = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Warhempl');
                    }
                );
        };

        function fetchPermissions() {
            UserService.fetchPermissions()
                .then(
                    function (d) {
                        $scope.permissions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                    }
                )
        };

        $scope.updateWarhempl = function (id, warhempl) {
            EmployeeService.updateEmployee(id, warhempl)
                .then(
                    function (d) {
                        $scope.changeReadonly();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                )
        };

        $scope.deleteWarhempl = function (id) {
            EmployeeService.deleteEmployee(id);
            $state.go('warhempls', {}, {reload: true});
        };
    }]);