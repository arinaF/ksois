app.controller('NewProblemController', ['$scope', 'ProblemService', 'EmployeeService', 'ClassifierService', 'DepartmentService',
    'UserService', 'ProjectService', '$state', '$stateParams', 'Notification', '$window',
    function ($scope, ProblemService, EmployeeService, ClassifierService, DepartmentService, UserService, ProjectService, $state, $stateParams, Notification, $window) {

        $scope.newProblemsList = [];

        $scope.actions = [];
        $scope.reasons = [];
        $scope.sources = [];
        $scope.warehouses = [];
        $scope.respDepartments = [];

        $scope.projects = [];
        $scope.drivers = [];
        $scope.managers = [];

        $scope.newProblem = {
            id: null,
            identifier: '',
            regDate: null,
            source: '',
            description: '',
            respDepartment: '',
            action: '',
            reason: '',
            informed: true,
            closeDate: null,

            sourceName: '',
            actionName: '',
            reasonName: '',

            invoiceId: null,
            invoiceIdentifier: '',
            invoiceDate: null,

            clientId: null,
            clientName: '',
            clientAddress: '',
            clientPosNr: null,

            projectId: null,
            projectCode: '',

            warehouseId: null,
            warehouseName: '',

            operatorId: null,
            operatorName: '',
            operatorSurname: '',

            driverId: null,
            driverName: '',
            driverSurname: '',

            managerId: '',
            managerName: '',
            managerSurname: '',

            routeId: null,
            routeCode: '',

            backInvId: null,
            backInvIdent: ''
        };

        addFirstProblem();
        fetchAutocomplete();

        function addFirstProblem() {
            if ($scope.newProblemsList.length == 0) {
                $scope.newProblemsList.push($scope.newProblem);
            }
        }

        $scope.addNewProblem = function () {
            $scope.addedProblem = $scope.newProblem;
            $scope.newProblemsList.push($scope.addedProblem);
        };

        function fetchAutocomplete() {
            ClassifierService.fetchAllClassifiers('SOURCE', null)
                .then(
                    function (d) {
                        $scope.sources = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Sources');
                    }
                );
            ClassifierService.fetchAllClassifiers('ACTION', null)
                .then(
                    function (d) {
                        $scope.actions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Actions');
                    }
                );
            ClassifierService.fetchAllClassifiers('REASON', null)
                .then(
                    function (d) {
                        $scope.reasons = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Reasons');
                    }
                );
            DepartmentService.fetchAllDepartments()
                .then(
                    function (d) {
                        $scope.respDepartments = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Responsive Departments');
                    }
                );
            DepartmentService.fetchAllDepartmentsByTypeOrCode("WAREHOUSE", null)
                .then(
                    function (d) {
                        $scope.warehouses = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Warehouses');
                    }
                );
            EmployeeService.fetchAllEmployees("TRANSPORT", null, null)
                .then(
                    function (d) {
                        $scope.drivers = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Drivers');
                    }
                );
            EmployeeService.fetchAllEmployees("SALES", null, null)
                .then(
                    function (d) {
                        $scope.managers = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Managers');
                    }
                );
            ProjectService.fetchAllProjects()
                .then(
                    function (d) {
                        $scope.projects = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Projects');
                    }
                );
        };

        $scope.fillPprData = function fillPprData(problem, invoice) {
            if (invoice.length > 9) {
                ProblemService.fetchPprData(invoice)
                    .then(
                        function (d) {
                            problem.invoiceId = d.invoice.id;
                            problem.invoiceIdentifier = d.invoice.identifier;
                            problem.invoiceDate = d.invoice.date;

                            problem.clientId = d.client.id;
                            problem.clientName = d.client.name;
                            problem.clientAddress = d.client.address;
                            problem.clientPosNr = d.client.posNr;

                            problem.projectId = d.project.id;
                            problem.projectCode = d.project.code;

                            problem.warehouseId = d.warehouse.id;
                            problem.warehouseName = d.warehouse.name;
                        },
                        function (errResponse) {
                            errResponse.data.errors.forEach(function (entry) {
                                Notification.error(entry);
                            });
                        }
                    )
            }
        };

        $scope.fillClientData = function fillPprData(problem, posNr) {
            if (posNr.length > 6) {
                ProblemService.fetchClientData(posNr)
                    .then(
                        function (d) {
                            problem.clientId = d.client.id;
                            problem.clientName = d.client.name;
                            problem.clientAddress = d.client.address;
                            problem.clientPosNr = d.client.posNr;
                        },
                        function (errResponse) {
                            errResponse.data.errors.forEach(function (entry) {
                                Notification.error(entry);
                            });
                        }
                    )
            }
        };

        $scope.saveProblems = function () {
            ProblemService.saveProblems($scope.newProblemsList)
                .then(
                    function (d) {
                        Notification.success("Veiksmīgi saglabāts");
                        $state.go("problems");
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                )
        };

        $scope.deleteProblem = function (id) {
            ProblemService.deleteProblem(id);
            $state.go('problems', {}, {reload: true});
        };
    }]);