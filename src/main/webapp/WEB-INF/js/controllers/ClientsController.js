app.controller('ClientsController', ['$scope', 'ClientService', '$state',
    function ($scope, ClientService, $state) {

        $scope.clients = [];

        fetchAllClients();

        function fetchAllClients() {
            ClientService.fetchAllClients()
                .then(
                    function (d) {
                        $scope.clients = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Clients');
                    }
                );
        }

        $scope.openClientProfile = function (id) {
            $state.go('client', {id: id});
        };

        $scope.createNewClient = function () {
            $state.go('newClient');
        };
    }]);