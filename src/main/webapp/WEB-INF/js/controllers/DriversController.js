app.controller('DriversController', ['$scope', 'EmployeeService', '$state',
    function ($scope, EmployeeService, $state) {

        $scope.drivers = [];

        $scope.departmentType = "TRANSPORT";

        fetchAllDrivers();

        function fetchAllDrivers() {
            EmployeeService.fetchAllEmployees($scope.departmentType, null, null)
                .then(
                    function (d) {
                        $scope.drivers = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Drivers');
                    }
                );
        }

        $scope.openDriverProfile = function (id) {
            $state.go('driver', {id: id});
        };

        $scope.createNewEmployee = function () {
            $state.go('newEmployee', {type: $scope.departmentType});
        };
    }]);