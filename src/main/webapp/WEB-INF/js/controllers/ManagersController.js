app.controller('ManagersController', ['$scope', 'EmployeeService', '$state',
    function ($scope, EmployeeService, $state) {

        $scope.managers = [];

        $scope.departmentType = "SALES";

        fetchAllManagers();

        function fetchAllManagers() {
            EmployeeService.fetchAllEmployees($scope.departmentType, null, null)
                .then(
                    function (d) {
                        $scope.managers = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Managers');
                    }
                );
        }

        $scope.openManagerProfile = function (id) {
            $state.go('manager', {id: id});
        };

        $scope.createNewEmployee = function () {
            $state.go('newEmployee', {type: $scope.departmentType});
        };

    }]);