app.run(function ($rootScope) {
    $rootScope.$on('mapInitialized', function (evt, map) {
        $rootScope.map = map;
        $rootScope.$apply();
    });
});

app.controller('MapController', ['$scope', function ($scope) {
    $scope.latlng = [56.9711614, 23.8500765];
    $scope.getpos = function (event) {
        $scope.latlng = [Number(event.latLng.lat().toFixed(6)), Number(event.latLng.lng().toFixed(6))];
    };
}]);