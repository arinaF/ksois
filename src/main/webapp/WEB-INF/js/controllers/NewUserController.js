app.controller('NewUserController', ['$scope', 'UserService', 'DepartmentService', 'EmployeeService', '$stateParams', '$window', 'Notification',
    function ($scope, UserService, DepartmentService, EmployeeService, $stateParams, $window, Notification) {

        $scope.newUser = {
            userId: null,
            employeeId: null,
            username: '',
            password: '',
            confirmedPassword: '',
            role: '',
            name: '',
            surname: '',
            departmentId: null,
            department: '',
            positionCode: '',
            positionName: '',
            regionCode: '',
            regionName: '',
            enabled: true
        };

        $scope.departments = [];
        $scope.employees = [];
        $scope.roles = ['USER', 'OPERATOR', 'MAIN_OPERATOR'];

        fetchAutocomplete();

        function fetchAutocomplete() {
            DepartmentService.fetchAllDepartments()
                .then(
                    function (d) {
                        $scope.departments = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Departments')
                    }
                );
        }

        $scope.fetchAllEmployees = function fetchAllEmployees(){
            EmployeeService.fetchAllEmployees(null, null, $scope.newUser.departmentId)
                .then(
                    function (d) {
                        $scope.employees = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Employees')
                    }
                )
        }


        $scope.saveNewUser = function saveNewUser() {
            UserService.saveUser($scope.newUser)
                .then(
                    function (d) {
                        Notification.success("Veiksmīgi saglabāts");
                        $window.history.back();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                )
        };
    }]);