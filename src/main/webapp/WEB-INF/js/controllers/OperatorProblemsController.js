app.controller('OperatorProblemsController', ['$scope', 'ProblemService', '$state',
    function ($scope, ProblemService, $state) {

        $scope.problems = [];

        fetchAllProblems();

        function fetchAllProblems() {
            ProblemService.fetchAllProblems(true)
                .then(
                    function (d) {
                        $scope.problems = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Operator Problems');
                    }
                );
        }

        $scope.openProblemDetails = function (id) {
            $state.go('problem', {id: id});
        };

        $scope.registerNewProblem = function () {
            $state.go('newProblem');
        };
    }]);