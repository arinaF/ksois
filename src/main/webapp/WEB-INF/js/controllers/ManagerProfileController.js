app.controller('ManagerProfileController', ['$scope', 'EmployeeService', 'ClassifierService', 'UserService', '$state', '$stateParams', 'Notification',
    function ($scope, EmployeeService, ClassifierService, UserService, $state, $stateParams, Notification) {

        $scope.readonly = true;
        $scope.regions = [];
        $scope.positions = [];
        $scope.contactInfoTypes = [];

        $scope.manager = {};
        $scope.managerContactInfo = [];
        $scope.permissions = [];

        fetchManager();
        fetchAutocomplete();
        fetchPermissions();

        $scope.addNewContactInfo = function addNewContactInfo() {
            $scope.newContact = {id: null, employeeId: $stateParams.id, type: '', contact: '', active: true};
            $scope.manager.employeeContactInfoList.push($scope.newContact);
        };

        $scope.deleteContactInfo = function deleteContactInfo(index) {
            $scope.manager.employeeContactInfoList.splice(index, 1);
        };


        $scope.changeReadonly = function changeReadonly() {
            $scope.readonly = !$scope.readonly;
        };

        $scope.hasPermission = function hasPermission(role) {
            return $.inArray(role, $scope.permissions) > -1;
        };

        $scope.cancelEdit = function cancelEdit() {
            fetchManager();
            $scope.changeReadonly();
        };

        $scope.cannotEdit = function () {
            return !(!$scope.readonly && $scope.hasPermission('MAIN_OPERATOR'));
        };

        function fetchAutocomplete() {
            ClassifierService.fetchAllClassifiers('REGION', null)
                .then(
                    function (d) {
                        $scope.regions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Regions');
                    }
                );
            ClassifierService.fetchAllClassifiers('POSITION', 'SALES')
                .then(
                    function (d) {
                        $scope.positions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Positions');
                    }
                );
            ClassifierService.fetchAllClassifiers('CONTACT_INFO_TYPE', null)
                .then(
                    function (d) {
                        $scope.contactInfoTypes = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching ContactInfoTypes');
                    }
                );
        }

        function fetchManager() {
            EmployeeService.fetchEmployee($stateParams.id)
                .then(
                    function (d) {
                        $scope.manager = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Manager');
                    }
                );
        };

        function fetchPermissions() {
            UserService.fetchPermissions()
                .then(
                    function (d) {
                        $scope.permissions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                    }
                )
        };

        $scope.updateManager = function (id, manager) {
            EmployeeService.updateEmployee(id, manager)
                .then(
                    function (d) {
                        Notification.success("Veiksmīgi saglabāts");
                        $scope.changeReadonly();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                )
        };

        $scope.deleteManager = function (id) {
            EmployeeService.deleteEmployee(id);
            $state.go('managers', {}, {reload: true});
        };
    }]);