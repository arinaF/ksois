app.controller('ProblemProfileController', ['$scope', 'ProblemService', 'EmployeeService', 'ClassifierService', 'DepartmentService',
    'UserService', 'ProjectService', 'ProblemCommentService', '$state', '$stateParams', 'Notification',
    function ($scope, ProblemService, EmployeeService, ClassifierService, DepartmentService, UserService,
              ProjectService, ProblemCommentService, $state, $stateParams, Notification) {

        $scope.readonly = true;
        $scope.actions = [];
        $scope.reasons = [];
        $scope.sources = [];
        $scope.warehouses = [];
        $scope.respDepartments = [];

        $scope.projects = [];
        $scope.drivers = [];
        $scope.managers = [];

        $scope.problem = {};
        $scope.permissions = [];
        $scope.editPermission = false;

        $scope.newComment = null;
        $scope.comments = [];

        $scope.userId;

        fetchProblem();
        fetchAutocomplete();
        fetchComments();


        $scope.canEdit = function () {
            return !(!$scope.readonly && $scope.editPermission);
        };

        $scope.changeReadonly = function changeReadonly() {
            $scope.readonly = !$scope.readonly;
        };

        $scope.hasPermission = function hasPermission(role) {
            return $.inArray(role, $scope.permissions) > -1;
        };

        $scope.cancelEdit = function cancelEdit() {
            fetchProblem();
            $scope.changeReadonly();
        };

        $scope.addNewComment = function () {
            $scope.newComment = {
                id: null,
                problemId: $scope.problem.id,
                date: null,
                userId: null,
                comText: '',
                emplId: null,
                authorName: '',
                authorSurname: '',
                authorPosition: '',
                authorDepartment: ''
            };
        };

        function fetchAutocomplete() {
            ClassifierService.fetchAllClassifiers('SOURCE', null)
                .then(
                    function (d) {
                        $scope.sources = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Sources');
                    }
                );
            ClassifierService.fetchAllClassifiers('ACTION', null)
                .then(
                    function (d) {
                        $scope.actions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Actions');
                    }
                );
            ClassifierService.fetchAllClassifiers('REASON', null)
                .then(
                    function (d) {
                        $scope.reasons = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Reasons');
                    }
                );
            DepartmentService.fetchAllDepartments()
                .then(
                    function (d) {
                        $scope.respDepartments = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Responsive Departments');
                    }
                );
            DepartmentService.fetchAllDepartmentsByTypeOrCode("WAREHOUSE", null)
                .then(
                    function (d) {
                        $scope.warehouses = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Warehouses');
                    }
                );
            EmployeeService.fetchAllEmployees("TRANSPORT", null, null)
                .then(
                    function (d) {
                        $scope.drivers = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Drivers');
                    }
                );
            EmployeeService.fetchAllEmployees("SALES", null, null)
                .then(
                    function (d) {
                        $scope.managers = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Managers');
                    }
                );
            ProjectService.fetchAllProjects()
                .then(
                    function (d) {
                        $scope.projects = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Projects');
                    }
                );
        }

        function fetchProblem() {
            ProblemService.fetchProblem($stateParams.id)
                .then(
                    function (d) {
                        $scope.problem = d;
                        fetchPermissions();
                    },
                    function (errResponse) {
                        console.error('Error while fetching Problem');
                    }
                );
        };

        function fetchComments() {
            ProblemCommentService.fetchAllComments($stateParams.id)
                .then(
                    function (d) {
                        $scope.comments = d;
                    },
                    function (errResponse) {
                        console.log('Eror while fetching Comments');
                    }
                )
        }

        function fetchPermissions() {
            UserService.fetchPermissions()
                .then(
                    function (d) {
                        $scope.permissions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                    }
                );
            UserService.fetchEditPermissions($scope.problem.operatorId)
                .then(
                    function (d) {
                        $scope.editPermission = d.hasEditPermission;
                    },
                    function (errResponse) {
                        console.error('Error while fetching edit permissions');
                    }
                );
            UserService.getUser()
                .then(
                    function (d) {
                        $scope.userId = d.id;
                    },
                    function (errResponse) {
                        console.error('Error while fetching User')
                    }
                )
        };

        $scope.updateProblem = function (id, problem) {
            ProblemService.updateProblem(id, problem)
                .then(
                    function (d) {
                        Notification.success("Veiksmīgi saglabāts");
                        $scope.changeReadonly();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                )
        };

        $scope.deleteProblem = function (id) {
            ProblemService.deleteProblem(id);
            $state.go('problems', {}, {reload: true});
        };

        $scope.saveOrUpdateComment = function () {
            ProblemCommentService.saveOrUpdateComment($scope.newComment)
                .then(
                    function (d) {
                        $scope.newComment = null;
                        Notification.success("Veiksmīgi saglabāts");
                        fetchComments();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                );
        };

        $scope.editComment = function (comment) {
            $scope.newComment = comment;
        };

        $scope.deleteComment = function (id) {
            ProblemCommentService.deleteComment(id);
            $state.reload();
            Notification.success("Veiksmīgi dzēsts");
        };

        $scope.cancelEditComment = function () {
            $scope.newComment = null;
            $state.reload();
        }
    }]);