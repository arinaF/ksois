app.controller('MainController', ['$scope', 'LogoutService', 'UserService', '$location', '$window',
    function ($scope, LogoutService, UserService, $location, $window) {
    $scope.hello = "Hello, KSOIS!";
    $scope.user;

    getUsername();

    $scope.logout = function logout() {
        LogoutService.logout()
            .then(
                function (d) {
                    $window.location.reload();
                },
                function (errResponse) {
                    console.error('Error while logging out');
                }
            );
    };

    function getUsername() {
        UserService.getUser()
            .then(
                function (d) {
                    $scope.user = d;
                },
                function (errResponse) {
                    console.error('Error while fetching username')
                }
            )
    };
}]);