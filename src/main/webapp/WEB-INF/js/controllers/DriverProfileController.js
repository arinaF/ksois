app.controller('DriverProfileController', ['$scope', 'EmployeeService', 'ClassifierService',
    'ProblemService', 'UserService', 'DepartmentService', '$state', '$stateParams', 'Notification',
    function ($scope, EmployeeService, ClassifierService, ProblemService, UserService,
              DepartmentService, $state, $stateParams, Notification) {

        $scope.readonly = true;
        $scope.departments = [];
        $scope.regions = [];
        $scope.positions = [];
        $scope.contactInfoTypes = [];

        $scope.driver = {};
        $scope.driverContactInfo = [];
        $scope.permissions = [];

        $scope.problems = [];

        fetchDriver();
        fetchAutocomplete();
        fetchPermissions();
        fetchAllProblems();

        $scope.addNewContactInfo = function addNewContactInfo() {
            $scope.newContact = {id: null, employeeId: $stateParams.id, type: '', contact: '', active: true};
            $scope.driver.employeeContactInfoList.push($scope.newContact);
        };

        $scope.deleteContactInfo = function deleteContactInfo(index) {
            $scope.driver.employeeContactInfoList.splice(index, 1);
        };


        $scope.changeReadonly = function changeReadonly() {
            $scope.readonly = !$scope.readonly;
        };

        $scope.hasPermission = function hasPermission(role) {
            return $.inArray(role, $scope.permissions) > -1;
        };

        $scope.cancelEdit = function cancelEdit() {
            fetchDriver();
            $scope.changeReadonly();
        };

        $scope.cannotEdit = function () {
            return !(!$scope.readonly && $scope.hasPermission('MAIN_OPERATOR'));
        };

        function fetchAutocomplete() {
            DepartmentService.fetchAllDepartmentsByTypeOrCode('TRANSPORT', null)
                .then(
                    function (d) {
                        $scope.departments = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Departments')
                    }
                );
            ClassifierService.fetchAllClassifiers('REGION', null)
                .then(
                    function (d) {
                        $scope.regions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Regions');
                    }
                );
            ClassifierService.fetchAllClassifiers('POSITION', 'TRANSPORT')
                .then(
                    function (d) {
                        $scope.positions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Positions');
                    }
                );
            ClassifierService.fetchAllClassifiers('CONTACT_INFO_TYPE', null)
                .then(
                    function (d) {
                        $scope.contactInfoTypes = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching ContactInfoTypes');
                    }
                );
        }

        function fetchDriver() {
            EmployeeService.fetchEmployee($stateParams.id)
                .then(
                    function (d) {
                        $scope.driver = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Driver');
                    }
                );
        };

        function fetchPermissions() {
            UserService.fetchPermissions()
                .then(
                    function (d) {
                        $scope.permissions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                    }
                )
        };

        function fetchAllProblems() {
            ProblemService.fetchDriverProblems($stateParams.id)
                .then(
                    function (d) {
                        $scope.problems = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Driver Problems');
                    }
                );
        }

        $scope.openProblemDetails = function (id) {
            $state.go('problem', {id: id});
        };


        $scope.updateDriver = function (id, driver) {
            EmployeeService.updateEmployee(id, driver)
                .then(
                    function (d) {
                        Notification.success("Veiksmīgi saglabāts");
                        $scope.changeReadonly();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                )
        };

        $scope.deleteDriver = function (id) {
            EmployeeService.deleteEmployee(id)
                .then(
                    function (d) {
                        Notification.success("Veiksmīgi dzēsts");
                        $state.go('drivers', {}, {reload: true});
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                );

        };
    }]);