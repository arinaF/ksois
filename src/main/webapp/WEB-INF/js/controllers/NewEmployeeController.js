app.controller('NewEmployeeController', ['$scope', 'ClassifierService', 'DepartmentService', 'EmployeeService', '$stateParams', '$window', 'Notification',
    function ($scope, ClassifierService, DepartmentService, EmployeeService, $stateParams, $window, Notification) {

        $scope.departmentType = $stateParams.type;

        $scope.newEmployee = {
            id: null,
            department: {
                id: null,
                type: '',
                code: '',
                region: '',
                address: '',
                lat: '',
                lng: '',
                name: ''
            },
            position: {
                id: null,
                parentCode: 'POSITION',
                code: '',
                name: '',
                type: $scope.departmentType
            },
            region: {
                id: null,
                parentCode: 'REGION',
                code: '',
                name: '',
                type: ''
            },
            name: '',
            surname: '',
            active: true,
            userId: null,
            employeeContactInfoList: []
        };

        $scope.departments = [];
        $scope.regions = [];
        $scope.positions = [];
        $scope.contactInfoTypes = [];

        fetchAutocomplete();

        function fetchAutocomplete() {
            DepartmentService.fetchAllDepartmentsByTypeOrCode($scope.departmentType, null)
                .then(
                    function (d) {
                        $scope.departments = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Departments')
                    }
                );
            ClassifierService.fetchAllClassifiers('REGION', null)
                .then(
                    function (d) {
                        $scope.regions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Regions');
                    }
                );
            ClassifierService.fetchAllClassifiers('POSITION', $scope.departmentType)
                .then(
                    function (d) {
                        $scope.positions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Positions');
                    }
                );
            ClassifierService.fetchAllClassifiers('CONTACT_INFO_TYPE', null)
                .then(
                    function (d) {
                        $scope.contactInfoTypes = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching ContactInfoTypes');
                    }
                );
        }

        $scope.addNewContactInfo = function addNewContactInfo() {
            $scope.newContact = {id: null, employeeId: null, type: '', contact: '', active: true};
            $scope.newEmployee.employeeContactInfoList.push($scope.newContact);
        };

        $scope.deleteContactInfo = function deleteContactInfo(index) {
            $scope.newEmployee.employeeContactInfoList.splice(index, 1);
        };

        $scope.saveNewEmployee = function saveNewEmployee() {
            EmployeeService.saveEmployee($scope.newEmployee)
                .then(
                    function (d) {
                        Notification.success("Veiksmīgi saglabāts");
                        $scope.createdId = d;
                        $window.history.back();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                )
        };
    }]);