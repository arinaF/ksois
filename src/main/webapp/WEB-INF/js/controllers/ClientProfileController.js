app.controller('ClientProfileController', ['$scope', 'ClassifierService', 'ClientService', 'UserService', 'ProblemService', '$state', '$stateParams', 'Notification',
    function ($scope, ClassifierService, ClientService, UserService, ProblemService, $state, $stateParams, Notification) {

        $scope.readonly = true;
        $scope.regions = [];
        $scope.contactInfoTypes = [];

        $scope.client = {};
        $scope.clientContactInfo = [];
        $scope.permissions = [];
        $scope.coordinates = [];

        $scope.problems = [];

        fetchClient();
        fetchAutocomplete();
        fetchPermissions();
        fetchAllProblems();

        $scope.addNewContactInfo = function addNewContactInfo() {
            $scope.newContact = {
                id: null,
                clientId: $stateParams.id,
                type: '',
                contact: '',
                contactPerson: '',
                active: true
            };
            $scope.client.clientContactInfoList.push($scope.newContact);
        };

        $scope.deleteContactInfo = function deleteContactInfo(index) {
            $scope.client.clientContactInfoList.splice(index, 1);
        };


        $scope.changeReadonly = function changeReadonly() {
            $scope.readonly = !$scope.readonly;
        };

        $scope.hasPermission = function hasPermission(role) {
            return $.inArray(role, $scope.permissions) > -1;
        };

        $scope.cancelEdit = function cancelEdit() {
            fetchClient();
            $scope.changeReadonly();
        };

        $scope.cannotEdit = function () {
            return !(!$scope.readonly && $scope.hasPermission('MAIN_OPERATOR'));
        };

        function fetchAutocomplete() {
            ClassifierService.fetchAllClassifiers('REGION', null)
                .then(
                    function (d) {
                        $scope.regions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Regions');
                    }
                );
            ClassifierService.fetchAllClassifiers('CONTACT_INFO_TYPE', null)
                .then(
                    function (d) {
                        $scope.contactInfoTypes = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching ContactInfoTypes');
                    }
                );
        }

        function fetchClient() {
            ClientService.fetchClient($stateParams.id)
                .then(
                    function (d) {
                        $scope.client = d;
                        $scope.coordinates = [d.lat, d.lng];
                    },
                    function (errResponse) {
                        console.error('Error while fetching Client');
                    }
                );
        };

        function fetchPermissions() {
            UserService.fetchPermissions()
                .then(
                    function (d) {
                        $scope.permissions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                    }
                )
        };

        function fetchAllProblems() {
            ProblemService.fetchClientProblems($stateParams.id)
                .then(
                    function (d) {
                        $scope.problems = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Client Problems');
                    }
                );
        }

        $scope.openProblemDetails = function (id) {
            $state.go('problem', {id: id});
        };

        $scope.updateClient = function (id, client) {
            ClientService.updateClient(id, client)
                .then(
                    function (d) {
                        $scope.changeReadonly();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                )
        };

        $scope.deleteClient = function (id) {
            ClientService.deleteClient(id)
                .then(
                    function (d) {
                        $scope.changeReadonly();
                        $state.go('clients', {}, {reload: true});
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                );
        };
    }]);