app.controller('RouteProfileController', ['$scope', 'RouteService', 'ReplannedInvoiceService', 'EmployeeService', 'UserService', '$state', '$stateParams', 'Notification',
    function ($scope, RouteService, ReplannedInvoiceService, EmployeeService, UserService, $state, $stateParams, Notification) {

        $scope.route = null;

        $scope.routeInvoices = [];

        $scope.permissions = [];

        $scope.editableRoute = null;

        $scope.path = [];

        fetchRoute();
        fetchRouteInvoices();
        fetchPermissions();
        fetchDrivers();


        function fetchRoute() {
            RouteService.fetchRoute($stateParams.id)
                .then(
                    function (d) {
                        $scope.route = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Route');
                    }
                );
        };

        function fetchRouteInvoices() {
            RouteService.fetchRouteInvoices($stateParams.id)
                .then(
                    function (d) {
                        $scope.routeInvoices = d;
                        d.forEach(function (entry){
                            $scope.path.push([entry.lat, entry.lng]);
                        });
                    },
                    function (errResponse) {
                        console.error('Error while fetching Route Invoices');
                    }
                );
        };

        function fetchPermissions() {
            UserService.fetchPermissions()
                .then(
                    function (d) {
                        $scope.permissions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                    }
                )
        };

        function fetchDrivers() {
            EmployeeService.fetchAllEmployees('TRANSPORT', null, null)
                .then(
                    function (d) {
                        $scope.drivers = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Drivers');
                    }
                )

        };

        $scope.hasPermission = function hasPermission(role) {
            return $.inArray(role, $scope.permissions) > -1;
        };

        $scope.rescheduleInvoices = function rescheduleInvoices() {
            ReplannedInvoiceService.saveReplannedInvoices($scope.routeInvoices);
        };

        $scope.updateRoute = function () {
            RouteService.updateRoute($scope.editableRoute)
                .then(
                    function (d) {
                        $scope.editableRoute = null;
                        $state.reload();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                );
        };

        $scope.cancelEdit = function () {
            $scope.editableRoute = null;
        };

        $scope.editRoute = function (route) {
            $scope.editableRoute = route;
        };
    }]);