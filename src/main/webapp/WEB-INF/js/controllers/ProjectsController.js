app.controller('ProjectsController', ['$scope', 'ProjectService', 'UserService', '$state', 'Notification',
    function ($scope, ProjectService, UserService, $state, Notification) {

        $scope.projects = [];

        $scope.editableProjects = [];

        $scope.permissions = [];

        fetchAllProjects();
        fetchPermissions();

        function fetchAllProjects() {
            ProjectService.fetchAllProjects()
                .then(
                    function (d) {
                        $scope.projects = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Projects');
                    }
                );
        };

        function fetchPermissions() {
            UserService.fetchPermissions()
                .then(
                    function (d) {
                        $scope.permissions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                    }
                )
        };

        $scope.hasPermission = function hasPermission(role) {
            return $.inArray(role, $scope.permissions) > -1;
        };

        $scope.saveOrUpdateProjects = function () {
            ProjectService.saveOrUpdateProjects($scope.editableProjects)
                .then(
                    function (d) {
                        $scope.editableProjects = [];
                        fetchAllProjects();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                );
        };

        $scope.cancelEdit = function () {
            $scope.editableProjects = [];
        };

        $scope.createNewProject = function () {
            $scope.newProject = {id: null, code: '', description: ''};
            $scope.editableProjects.push($scope.newProject);
        };

        $scope.editProject = function (project) {
            $scope.editableProjects.push(project);
        };

        $scope.deleteProject = function (id) {
            ProjectService.deleteProject(id);
            $state.reload();
        }

    }]);