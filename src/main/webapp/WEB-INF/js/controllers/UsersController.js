app.controller('UsersController', ['$scope', 'UserService', 'EmployeeService', '$state', 'Notification',
    function ($scope, UserService, EmployeeService, $state, Notification) {

        $scope.users = [];

        $scope.editableUser = null;

        $scope.permissions = [];

        $scope.roles = ['USER', 'OPERATOR', 'MAIN_OPERATOR'];

        fetchAllUsers();
        fetchPermissions();

        function fetchAllUsers() {
            UserService.fetchAllUsers()
                .then(
                    function (d) {
                        $scope.users = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Users');
                    }
                );
        };

        function fetchPermissions() {
            UserService.fetchPermissions()
                .then(
                    function (d) {
                        $scope.permissions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                    }
                )
        };

        $scope.hasPermission = function hasPermission(role) {
            return $.inArray(role, $scope.permissions) > -1;
        };

        $scope.updateUser = function () {
            UserService.updateUser($scope.editableUser)
                .then(
                    function (d) {
                        $scope.editableUser = null;
                        Notification.success("Veiksmīgi saglabāts");
                        fetchAllUsers();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                );
        };

        $scope.cancelEdit = function () {
            $scope.editableUser = null;
        };

        $scope.createNewEmployeeUser = function () {
            $state.go('newUser');
        };

        $scope.editUser = function (user) {
            $scope.editableUser = user;
        };

        $scope.deleteUser = function (id) {
            UserService.deleteUser(id);
            $state.reload();
        }

    }]);