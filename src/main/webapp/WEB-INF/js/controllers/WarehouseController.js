app.controller('WarehouseController', ['$scope', 'EmployeeService', 'DepartmentService', '$state', 'NgMap',
    function ($scope, EmployeeService, DepartmentService, $state, NgMap) {

        $scope.departmentType = "WAREHOUSE";

        $scope.warehouses = [];

        $scope.warehouseEmployees = [];

        $scope.employees = [];

        $scope.coordinates = [];

        $scope.address = '';

        fetchAllWarehouses();

        function fetchAllWarehouses() {
            DepartmentService.fetchAllDepartmentsByTypeOrCode($scope.departmentType, null)
                .then(
                    function (d) {
                        $scope.warehouses = d;
                        fetchWarehouseEmployees();
                    },
                    function (errResponse) {
                        console.error('Error while fetching Warehouses');
                    }
                );
        }

        function fetchWarehouseEmployees() {
            $scope.warehouses.forEach(function (entry) {
                EmployeeService.fetchAllEmployees(entry.type, entry.code, null)
                    .then(
                        function (d) {
                            $scope.warehouseEmployees.push({
                                departmentCode: entry.code,
                                employees: d
                            });
                            if ($scope.employees.length === 0) {
                                $scope.getInfo($scope.warehouses[0]);
                            }
                        },
                        function (errResponse) {
                            console.error('Error while fetching Warehouse Employees');
                        }
                    );
            });
        }

        $scope.getInfo = function (warh) {
            $scope.warehouseEmployees.forEach(function (entry) {
                if (entry.departmentCode === warh.code) {
                    $scope.employees = entry.employees;
                }
            });
            $scope.coordinates = [warh.lat, warh.lng];
            $scope.address = warh.address;
        };

        $scope.openWarehouseEmployeeProfile = function (id) {
            $state.go('warehouseEmployee', {id: id});
        };

        $scope.createNewEmployee = function () {
            $state.go('newEmployee', {type: $scope.departmentType});
        };
    }]);