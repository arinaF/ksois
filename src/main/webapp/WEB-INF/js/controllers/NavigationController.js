app.controller('NavigationController', ['$scope', function ($scope) {

    $scope.collapsedName = 'Izvēlne';

    $scope.collapseMenu = function (index) {
        $scope.selectedId = $scope.sideMenu[index].collapseId;
        $("#" + $scope.selectedId).slideToggle("slow");
    };

    $scope.sideMenu = [
        {
            collapseId: 'collapseProblems',
            mainOption: 'Problēmas',
            options: [
                {
                    option: 'Manas reģistrētas',
                    icon: 'glyphicon glyphicon-pencil',
                    link: '/myProblems'
                },
                {
                    option: 'Visas problēmas',
                    icon: 'glyphicon glyphicon-th-list',
                    link: '/problems'
                }
            ]
        },
        {
            collapseId: 'collapseTransport',
            mainOption: 'Transports',
            options: [
                {
                    option: 'Maršruti',
                    icon: 'glyphicon glyphicon-map-marker',
                    link: '/routes'
                },
                {
                    option: 'Šoferi',
                    icon: 'glyphicon glyphicon-road',
                    link: '/drivers'
                },
                {
                    option: 'Pārplānots',
                    icon: 'glyphicon glyphicon-calendar',
                    link: '/rescheduled'
                }
            ]
        },
        {
            collapseId: 'collapseSales',
            mainOption: 'Pārdošana',
            options: [
                {
                    option: 'Klienti',
                    icon: 'glyphicon glyphicon-shopping-cart',
                    link: '/clients'
                },
                {
                    option: 'Menedžeri',
                    icon: 'glyphicon glyphicon-eur',
                    link: '/managers'
                },
                {
                    option: 'Projekti',
                    icon: 'glyphicon glyphicon-briefcase',
                    link: '/projects'
                }
            ]
        },
        {
            collapseId: 'collapseWarehouse',
            mainOption: 'Noliktava',
            options: [
                {
                    option: 'Kontakti',
                    icon: 'glyphicon glyphicon-earphone',
                    link: '/warehouses'
                }
            ]
        },
        {
            collapseId: 'collapseAdmin',
            mainOption: 'Administrēšana',
            options: [
                {
                    option: 'Lietotāji',
                    icon: 'glyphicon glyphicon-user',
                    link: '/users'
                },
                {
                    option: 'Masveida ielade',
                    icon: 'glyphicon glyphicon-cloud-upload'
                }
            ]
        },
        {
            collapseId: 'collapseReports',
            mainOption: 'Statistika un atskaites',
            options: [
                {
                    option: 'Statistika',
                    icon: 'glyphicon glyphicon-stats'
                },
                {
                    option: 'Atskaites',
                    icon: 'glyphicon glyphicon-list-alt'
                }
            ]
        }
    ];
}]);