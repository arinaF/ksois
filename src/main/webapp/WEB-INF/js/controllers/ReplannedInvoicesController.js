app.controller('ReplannedInvoicesController', ['$scope', 'ReplannedInvoiceService', 'UserService', '$state', 'Notification',
    function ($scope, ReplannedInvoiceService, UserService, $state, Notification) {

        $scope.replannedInvoices = [];

        $scope.editableInvoice = null;

        $scope.permissions = [];

        fetchAllInvoices();
        fetchPermissions();

        function fetchAllInvoices() {
            ReplannedInvoiceService.fetchAllReplannedInvoices()
                .then(
                    function (d) {
                        $scope.replannedInvoices = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching Replanned Invoices');
                    }
                );
        };

        function fetchPermissions() {
            UserService.fetchPermissions()
                .then(
                    function (d) {
                        $scope.permissions = d;
                    },
                    function (errResponse) {
                        console.error('Error while fetching permissions');
                    }
                )
        };

        $scope.hasPermission = function hasPermission(role) {
            return $.inArray(role, $scope.permissions) > -1;
        };

        $scope.updateReplannedInvoice = function () {
            ReplannedInvoiceService.updateReplannedInvoice($scope.editableInvoice.id, $scope.editableInvoice)
                .then(
                    function (d) {
                        $scope.editableInvoice = null;
                        fetchAllInvoices();
                    },
                    function (errResponse) {
                        errResponse.data.errors.forEach(function (entry) {
                            Notification.error(entry);
                        });
                    }
                );
        };

        $scope.cancelEdit = function () {
            $scope.editableInvoice = null;
        };

        $scope.editInvoice = function (invoice) {
            $scope.editableInvoice = invoice;
        };

        $scope.deleteInvoice = function (id) {
            ReplannedInvoiceService.deleteReplannedInvoice(id);
            $state.reload();
        }

    }]);