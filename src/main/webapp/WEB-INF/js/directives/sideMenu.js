app.directive('sideMenu', function(){
    return {
        restrict: 'E',
        templateUrl: 'js/directives/sideMenu.html'
    };
});